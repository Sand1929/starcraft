You've found an AI that plays Starcraft on its own! 

Running it is actually fairly difficult, since you have to inject your code into Starcraft. I'll add instructions to build and run the AI later, though if you really want to try right now, you can use the instructions at starcraftai.com for injecting code into Starcraft with BWAPI/Chaoslauncher.