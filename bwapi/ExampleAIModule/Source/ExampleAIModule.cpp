#include "ExampleAIModule.h"
#include "BuildOrderManager.h"
#include "ResourceManager.h"
#include "CombatManager.h"
#include "StrategyManager.h"
#include "ScoutManager.h"
#include <iostream>

using namespace BWAPI;
using namespace Filter;

void ExampleAIModule::onStart()
{
  Broodwar->sendText("I'm a pastry chef! I make gastronomical confections!!!");

  StrategyManager::getInstance().decideStrategy();

  Broodwar << StrategyManager::getInstance().getStrategy() << std::endl;

  // Print the map name.
  // BWAPI returns std::string when retrieving a string, don't forget to add .c_str() when printing!
  
  // Enable the UserInput flag, which allows us to control the bot and type messages.
  Broodwar->enableFlag(Flag::UserInput);

  // Uncomment the following line and the bot will know about everything through the fog of war (cheat).
  //Broodwar->enableFlag(Flag::CompleteMapInformation);

  // Set the command optimization level so that common commands can be grouped
  // and reduce the bot's APM (Actions Per Minute).
  Broodwar->setCommandOptimizationLevel(2);

  // Check if this is a replay
  if ( Broodwar->isReplay() )
  {

    // Announce the players in the replay
    Broodwar << "The following players are in this replay:" << std::endl;
    
    // Iterate all the players in the game using a std:: iterator
    Playerset players = Broodwar->getPlayers();
    for(auto p : players)
    {
      // Only print the player if they are not an observer
      if ( !p->isObserver() )
        Broodwar << p->getName() << ", playing as " << p->getRace() << std::endl;
    }

  }
  else // if this is not a replay
  {
    // Retrieve you and your enemy's races. enemy() will just return the first enemy.
    // If you wish to deal with multiple enemies then you must use enemies().
    if ( Broodwar->enemy() ) // First make sure there is an enemy
      Broodwar << "The matchup is " << Broodwar->self()->getRace() << " vs " << Broodwar->enemy()->getRace() << std::endl;
  }

}

void ExampleAIModule::onEnd(bool isWinner)
{
	StrategyManager::getInstance().updateStrategy(isWinner);
  // Called when the game ends
  if ( isWinner )
  {
    // Log your win here!
  }
}

void ExampleAIModule::onFrame()
{
  // Called once every game frame

  // Display the game frame rate as text in the upper left area of the screen
  Broodwar->drawTextScreen(200, 0,  "FPS: %d", Broodwar->getFPS() );
  Broodwar->drawTextScreen(200, 20, "Average FPS: %f", Broodwar->getAverageFPS() );

  // Return if the game is a replay or is paused
  if ( Broodwar->isReplay() || Broodwar->isPaused() || !Broodwar->self() )
    return;

  // Prevent spamming by only running our onFrame once every number of latency frames.
  // Latency frames are the number of frames before commands are processed.
  if ( Broodwar->getFrameCount() % Broodwar->getLatencyFrames() != 0 )
    return;

  // create new BuildOrderManager to determine what to build next
  BuildOrderManager::getInstance().executeBuildOrder();

  // issue orders to units
  CombatManager::getInstance().giveOrders();

  // keep track of time on resource locks
  ResourceManager::getInstance().checkExpirations();
}

void ExampleAIModule::onSendText(std::string text)
{

  // Send the text to the game if it is not being processed.
  Broodwar->sendText("%s", text.c_str());


  // Make sure to use %s and pass the text as a parameter,
  // otherwise you may run into problems when you use the %(percent) character!

}

void ExampleAIModule::onReceiveText(BWAPI::Player player, std::string text)
{
  // Parse the received text
  Broodwar << player->getName() << " said \"" << text << "\"" << std::endl;
}

void ExampleAIModule::onPlayerLeft(BWAPI::Player player)
{
  // Interact verbally with the other players in the game by
  // announcing that the other player has left.
  Broodwar->sendText("Goodbye %s!", player->getName().c_str());
}

void ExampleAIModule::onNukeDetect(BWAPI::Position target)
{

  // Check if the target is a valid position
  if ( target )
  {
    // if so, print the location of the nuclear strike target
    Broodwar << "Nuclear Launch Detected at " << target << std::endl;
  }
  else 
  {
    // Otherwise, ask other players where the nuke is!
    Broodwar->sendText("Where's the nuke?");
  }

  // You can also retrieve all the nuclear missile targets using Broodwar->getNukeDots()!
}

void ExampleAIModule::onUnitDiscover(BWAPI::Unit unit)
{

}

void ExampleAIModule::onUnitEvade(BWAPI::Unit unit)
{
}

void ExampleAIModule::onUnitShow(BWAPI::Unit unit)
{
	UnitType ut = unit->getType();

	// keep track of enemy units
	if (Broodwar->self()->isEnemy(unit->getPlayer()))
	{
		// keep track of how many of each unit the enemy has
		if (ScoutManager::getInstance().enemyHiddenCount[ut]) {
			ScoutManager::getInstance().enemyHiddenCount[ut] -= 1;
		}
		else {
			ScoutManager::getInstance().enemyCount[ut] += 1;
		}

		// exclude gas-collecting buildings, because the API glitches
		if (ut.isBuilding() && unit->isCompleted() && ut != UnitTypes::Protoss_Assimilator && ut != UnitTypes::Terran_Refinery && ut != UnitTypes::Zerg_Extractor)
		{
			// keep track of the positions of enemy buildings
			ScoutManager::getInstance().enemyBuildingLocations.insert(unit->getPosition());
			for (auto &enemyPosition : ScoutManager::getInstance().enemyBuildingLocations)
			{
				Broodwar << enemyPosition;
			}
			Broodwar << std::endl;
			// if the unit is an enemy stationary detector, mark it as such
			if (ut.isDetector() && unit->isCompleted())
			{
				CombatManager::getInstance().detectors.insert(unit->getPosition());
			}
		}
		if (ut.isBuilding() && unit->isCompleted() && ut.canAttack())
		{
			CombatManager::getInstance().enemyFortifications[unit->getPosition()] = unit->getType();
		}
	}
	// keep track of mineral and gas locations
	else if (ut == UnitTypes::Resource_Mineral_Field || ut == UnitTypes::Resource_Mineral_Field_Type_2 || ut == UnitTypes::Resource_Mineral_Field_Type_3)
	{
		ScoutManager::getInstance().mineralLocations.insert(unit->getPosition());
	}
	else if (ut == UnitTypes::Resource_Vespene_Geyser)
	{
		ScoutManager::getInstance().gasLocations.insert(unit->getPosition());
	}
}

void ExampleAIModule::onUnitHide(BWAPI::Unit unit)
{
	// keep track of enemy units that we've counted but can't see anymore
	if (Broodwar->self()->isEnemy(unit->getPlayer())) {
		UnitType ut = unit->getType();

		ScoutManager::getInstance().enemyHiddenCount[ut] += 1;
	}
}

void ExampleAIModule::onUnitCreate(BWAPI::Unit unit)
{
	if (unit->getPlayer() == Broodwar->self())
	{
		// release lock on unit's resources once it is being built
		ResourceManager::getInstance().releaseLockedResources(unit->getType());

		// if unit is a gateway, set rally point to start location so Zealots can group up
		if (unit->getType() == UnitTypes::Protoss_Gateway)
		{
			TilePosition rallyPoint = Broodwar->self()->getStartLocation();
			unit->setRallyPoint(Position(rallyPoint));
		}
		// if unit is a building, release lock on land
		if (unit->getType().isBuilding())
		{
			ResourceManager::getInstance().releaseLockedLand(unit->getTilePosition());
		}
	}
  if ( Broodwar->isReplay() )
  {
    // if we are in a replay, then we will print out the build order of the structures
    if ( unit->getType().isBuilding() && !unit->getPlayer()->isNeutral() )
    {
      int seconds = Broodwar->getFrameCount()/24;
      int minutes = seconds/60;
      seconds %= 60;
      Broodwar->sendText("%.2d:%.2d: %s creates a %s", minutes, seconds, unit->getPlayer()->getName().c_str(), unit->getType().c_str());
    }
  }
}

void ExampleAIModule::onUnitDestroy(BWAPI::Unit unit)
{
	UnitType ut = unit->getType();

	// increase squad size if too many of your fighters are destroyed
	if (unit->getPlayer() == Broodwar->self() && !ut.isWorker() && !ut.isBuilding() && unit->isCompleted())
	{
		CombatManager::getInstance().toggleIncreaseSquadSize();
	}
	
	// unmark destroyed units
	if (Broodwar->self()->isEnemy(unit->getPlayer()))
	{
		ScoutManager::getInstance().enemyCount[ut] = std::max(ScoutManager::getInstance().enemyCount[ut] - 1, 0);

		if (unit->getType().isBuilding() && ut != UnitTypes::Protoss_Assimilator && ut != UnitTypes::Terran_Refinery && ut != UnitTypes::Zerg_Extractor)
		{
			ScoutManager::getInstance().enemyBuildingLocations.erase(unit->getPosition());
			for (auto &enemyPosition : ScoutManager::getInstance().enemyBuildingLocations)
			{
				Broodwar << enemyPosition;
			}
			Broodwar << std::endl;

			if (unit->getType().isDetector())
			{
				CombatManager::getInstance().detectors.erase(unit->getPosition());
			}
		}
		if (unit->getType().isBuilding() && unit->isCompleted() && unit->getType().canAttack())
		{
			CombatManager::getInstance().enemyFortifications.erase(unit->getPosition());
		}
	}
}

void ExampleAIModule::onUnitMorph(BWAPI::Unit unit)
{
  if ( Broodwar->isReplay() )
  {
    // if we are in a replay, then we will print out the build order of the structures
    if ( unit->getType().isBuilding() && !unit->getPlayer()->isNeutral() )
    {
      int seconds = Broodwar->getFrameCount()/24;
      int minutes = seconds/60;
      seconds %= 60;
      Broodwar->sendText("%.2d:%.2d: %s morphs a %s", minutes, seconds, unit->getPlayer()->getName().c_str(), unit->getType().c_str());
    }
  }
}

void ExampleAIModule::onUnitRenegade(BWAPI::Unit unit)
{
}

void ExampleAIModule::onSaveGame(std::string gameName)
{
  Broodwar << "The game was saved to \"" << gameName << "\"" << std::endl;
}

void ExampleAIModule::onUnitComplete(BWAPI::Unit unit)
{
}
