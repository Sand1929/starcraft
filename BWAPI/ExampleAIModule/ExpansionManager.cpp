#include "ExpansionManager.h"
#include "ScoutManager.h"
#include "ResourceManager.h"
#include <limits>

using namespace BWAPI;

ExpansionManager::ExpansionManager()
{	
	numExpansions = 0;
}

TilePosition ExpansionManager::pickExpansion()
{
	// iterate through mineral patches to find the closest one without a nexus near it
	Position closestMineralPatch;
	int bestDistance = std::numeric_limits<int>::max();
	for (auto &mineral : ScoutManager::getInstance().mineralLocations)
	{
		// an expansion can't be too close to an existing nexus
		bool tooClose = false;
		for (auto &unit : Broodwar->self()->getUnits())
		{
			if (unit->getType() == UnitTypes::Protoss_Nexus && unit->getDistance(mineral) < 500)
			{
				tooClose = true;
				break;
			}
		}
		if (tooClose)
		{
			break;
		}

		int distance = mineral.getDistance(Position(Broodwar->self()->getStartLocation()));
		if (distance < bestDistance)
		{
			closestMineralPatch = mineral;
			bestDistance = distance;
		}
	}
	if (bestDistance < std::numeric_limits<int>::max())
	{
		return TilePosition(closestMineralPatch);
	}
	else
	{
		return Broodwar->self()->getStartLocation();
	}
}

bool ExpansionManager::giveExpansionOrders(Unit unit)
{
	Broodwar << "EXPANDING" << std::endl;
	// get the location of the mineral patch for the expansion
	TilePosition closestMineralPatch = pickExpansion();
	// if we can't find one, stop
	if (closestMineralPatch == Broodwar->self()->getStartLocation())
	{
		Broodwar << "No good expansions" << std::endl;
		return false;
	}

	// find a position near the mineral patch to build the expansion
	TilePosition targetBuildLocation = Broodwar->getBuildLocation(UnitTypes::Protoss_Nexus, closestMineralPatch);
	if (targetBuildLocation)
	{
		if (Broodwar->isExplored(targetBuildLocation))
		{
			UnitType u1 = UnitTypes::Protoss_Nexus;
			// Register an event that draws the target build location
			Broodwar->registerEvent([targetBuildLocation, u1](Game*)
			{
				Broodwar->drawBoxMap(Position(targetBuildLocation),
					Position(targetBuildLocation + u1.tileSize()),
					Colors::Blue);
			},
				nullptr,  // condition
				u1.buildTime() + 100);  // frames to run

			// Order the builder to construct the supply structure
			// lock resources for the build
			if (ResourceManager::getInstance().lockResources(UnitTypes::Protoss_Nexus) && ResourceManager::getInstance().lockLand(targetBuildLocation, targetBuildLocation + u1.tileSize()))
			{
				Broodwar << "Can build: " << UnitTypes::Protoss_Nexus << std::endl;
				if (unit->build(UnitTypes::Protoss_Nexus, targetBuildLocation))
				{
					++numExpansions;
					// we don't need to scout anymore
					ScoutManager::getInstance().stopScouting();
					return true;
				}
				else
				{
					Broodwar << Broodwar->getLastError() << std::endl;
					ResourceManager::getInstance().releaseLockedResources(UnitTypes::Protoss_Nexus);
					ResourceManager::getInstance().releaseLockedLand(targetBuildLocation);
					return false;
				}
			}
		}
		else
		{
			unit->move(Position(targetBuildLocation));
			return false;
		}
	}
	return false;
}

int ExpansionManager::getNumExpansions()
{
	return numExpansions;
}