#include "BuildOrderManager.h"
#include "StrategyManager.h"
#include "ResourceManager.h"
#include "ExpansionManager.h"
#include <queue>
#include <fstream>
#include <math.h>

using namespace BWAPI;

bool operator== (const MacroGameState &s1, const MacroGameState &s2)
{
	if (s1.minerals != s2.minerals)
	{
		return false;
	}
	if (s1.gas != s2.gas)
	{
		return false;
	}
	if (s1.supplyUsed != s2.supplyUsed)
	{
		return false;
	}
	if (s1.supplyTotal != s2.supplyTotal)
	{
		return false;
	}
	if (s1.mineralWorkers != s2.mineralWorkers)
	{
		return false;
	}
	if (s1.gasWorkers != s2.gasWorkers)
	{
		return false;
	}
	if (s1.unitCounts != s2.unitCounts)
	{
		return false;
	}
	if (s1.unitsInProgressCounts != s2.unitsInProgressCounts)
	{
		return false;
	}
	if (s1.upgradesInProgressCounts != s2.upgradesInProgressCounts)
	{
		return false;
	}
	if (s1.techsInProgressCounts != s2.techsInProgressCounts)
	{
		return false;
	}
	if (s1.unitBusyTimes != s2.unitBusyTimes)
	{
		return false;
	}
	if (s1.goalUnits != s2.goalUnits)
	{
		return false;
	}
	if (s1.goalUpgrades != s2.goalUpgrades)
	{
		return false;
	}
	if (s1.goalTechs != s2.goalTechs)
	{
		return false;
	}
	return true;
}

struct MacroGameStateHasher
{
	std::size_t operator()(const MacroGameState &state) const
	{
		// combine hash values for different variables of state
		// http://stackoverflow.com/a/1646913/126995
		size_t result = 1009;
		result = result * 9176 + std::hash<int>()(state.minerals);
		result = result * 9176 + std::hash<int>()(state.gas);
		result = result * 9176 + std::hash<int>()(state.supplyTotal);
		result = result * 9176 + std::hash<int>()(state.supplyUsed);
		result = result * 9176 + std::hash<int>()(state.unitBusyTimes.size());
		result = result * 9176 + std::hash<int>()(state.unitsInProgressCounts.size());
		result = result * 9176 + std::hash<int>()(state.upgradesInProgressCounts.size());
		result = result * 9176 + std::hash<int>()(state.techsInProgressCounts.size());
		result = result * 9176 + std::hash<int>()(state.goalUnits.size());
		result = result * 9176 + std::hash<int>()(state.goalUpgrades.size());
		result = result * 9176 + std::hash<int>()(state.goalTechs.size());
		result = result * 9176 + std::hash<int>()(state.unitCounts.size());
		return result;
	}
};

/* Constructor to create a game state representing the current game state */
MacroGameState::MacroGameState(std::map<BWAPI::UnitType, int> startGoalUnits, std::map<BWAPI::UpgradeType, int> startGoalUpgrades, std::set<BWAPI::TechType> startGoalTechs)
{
	gameTime = Broodwar->getFrameCount();
	minerals = Broodwar->self()->minerals();
	gas = Broodwar->self()->gas();
	supplyUsed = Broodwar->self()->supplyUsed();
	supplyTotal = Broodwar->self()->supplyTotal();
	mineralWorkers = 0;
	gasWorkers = 0;
	goalUnits = std::map<BWAPI::UnitType, int>(startGoalUnits);
	goalUpgrades = std::map<BWAPI::UpgradeType, int>(startGoalUpgrades);
	goalTechs = std::set<BWAPI::TechType>(startGoalTechs);
	std::vector < std::pair<BWAPI::UnitType, int> > unitBusyTimes;

	for (auto &u : Broodwar->self()->getUnits())
	{
		// Ignore the unit if it no longer exists
		// Make sure to include this block when handling any Unit pointer!
		if (!u->exists())
			continue;

		// if unit is complete
		if (u->isCompleted())
		{
			++unitCounts[u->getType()];
		}
		else
		{
			std::pair<BWAPI::UnitType, int> unitProgress(u->getType(), u->getRemainingBuildTime());
			unitsInProgressCounts.push_back(unitProgress);
			continue;
		}

		// if the unit is a worker unit
		if (u->getType().isWorker())
		{
			if (u->isGatheringMinerals() || u->isCarryingMinerals())
			{
				++mineralWorkers;
			}
			else if (u->isGatheringGas() || u->isCarryingGas())
			{
				++gasWorkers;
			}
		}

		// if unit is busy researching
		if (u->isResearching())
		{
			std::pair<UnitType, int> unitTime(u->getType(), u->getRemainingResearchTime());
			unitBusyTimes.push_back(unitTime);
			techsInProgressCounts[u->getTech()] = u->getRemainingResearchTime();
		}
		else if (u->isUpgrading()) 
		{
			std::pair<UnitType, int> unitTime(u->getType(), u->getRemainingUpgradeTime());
			unitBusyTimes.push_back(unitTime);
			upgradesInProgressCounts[u->getUpgrade()] = u->getRemainingUpgradeTime();
		}
		else if (u->isTraining())
		{
			std::pair<UnitType, int> unitTime(u->getType(), u->getRemainingTrainTime());
			unitBusyTimes.push_back(unitTime);
		}
	}
	// fill in units in progress
	for (auto &unitCount : ResourceManager::getInstance().unitsInProgressCounts)
	{
		for (int i = 0; i < unitCount.second.first; ++i)
		{
			std::pair<BWAPI::UnitType, int> unitProgress(unitCount.first, unitCount.first.buildTime() + 40);
			unitsInProgressCounts.push_back(unitProgress);
		}
	}
	// sort units in progress by waiting time
	std::sort(unitsInProgressCounts.begin(), unitsInProgressCounts.end(), [](const std::pair<BWAPI::UnitType, int> &left, const std::pair<BWAPI::UnitType, int> &right) {
		return left.second < right.second;
	});
}

/* Constructor to transition to new game state */
MacroGameState::MacroGameState(const MacroGameState &currentState, BuildOrderAction action)
{
	// first make the next state a copy of the current state
	gameTime = currentState.gameTime;
	minerals = currentState.minerals - action.mineralCost;
	gas = currentState.gas - action.gasCost;
	supplyUsed = currentState.supplyUsed + action.supplyCost;
	supplyTotal = currentState.supplyTotal;
	mineralWorkers = currentState.mineralWorkers;
	gasWorkers = currentState.gasWorkers;
	unitCounts = std::map<BWAPI::UnitType, int>(currentState.unitCounts);
	unitsInProgressCounts = std::vector<std::pair<BWAPI::UnitType, int>>(currentState.unitsInProgressCounts);
	upgradesInProgressCounts = std::map<BWAPI::UpgradeType, int>(currentState.upgradesInProgressCounts);
	techsInProgressCounts = std::map<BWAPI::TechType, int>(currentState.techsInProgressCounts);
	unitBusyTimes = std::vector<std::pair<BWAPI::UnitType, int>>(currentState.unitBusyTimes);
	goalUnits = std::map<BWAPI::UnitType, int>(currentState.goalUnits);
	goalUpgrades = std::map<BWAPI::UpgradeType, int>(currentState.goalUpgrades);
	goalTechs = std::set<BWAPI::TechType>(currentState.goalTechs);

	// determine time until action is possible
	int minTime = 0;
	// if we don't have enough minerals, estimate time until we do
	if (minerals < 0)
	{
		minTime = std::ceil(-1 * minerals / (mineralWorkers * 0.045));
	}
	if (gas < 0)
	{
		int gasTime = std::ceil(-1 * gas / (gasWorkers * 0.07));
		if (gasTime > minTime)
		{
			minTime = gasTime;
		}
	}
	// if we don't have enough supply, estimate time until we do
	if (supplyUsed > supplyTotal)
	{
		int maxSupplyTime = 0;
		int supplyNeeded = supplyUsed - supplyTotal;
		for (auto &unitCount : unitsInProgressCounts)
		{
			if (unitCount.first.supplyProvided() > 0)
			{
				supplyNeeded -= unitCount.first.supplyProvided();
				maxSupplyTime = std::max(maxSupplyTime, unitCount.second);
				if (supplyNeeded <= 0)
				{
					break;
				}
			}
		}
		if (supplyNeeded > 0)
		{
			for (auto &unitCount : ResourceManager::getInstance().unitsInProgressCounts)
			{
				if (unitCount.first.supplyProvided() > 0)
				{
					supplyNeeded -= unitCount.first.supplyProvided() * unitCount.second.first;
					maxSupplyTime = std::max(maxSupplyTime, unitCount.first.buildTime() + 40);
					if (supplyNeeded <= 0)
					{
						break;
					}
				}
			}
		}
		minTime = std::max(minTime, maxSupplyTime);
	}
	// check if there are required units we are still building
	std::map<BWAPI::UnitType, int> requiredUnits = action.requiredUnits;
	for (auto &unitCount : unitCounts)
	{
		// if we've satisfied all requirements, stop checking
		if (requiredUnits.empty())
		{
			break;
		}
		// remove units we have from required units
		if (requiredUnits.count(unitCount.first) > 0)
		{
			if (unitCount.second >= requiredUnits[unitCount.first])
			{
				requiredUnits.erase(unitCount.first);
			}
			else
			{
				requiredUnits[unitCount.first] -= unitCount.second;
			}
		}
	}
	// if we are still building required units, calculate time until built
	if (!requiredUnits.empty())
	{
		int minBuildTime = 0;
		for (auto &unitCount : unitsInProgressCounts)
		{
			// if this unit is required, remove it and update minBuildTime
			if (requiredUnits.erase(unitCount.first) > 0)
			{
				if (unitCount.second > minBuildTime)
				{
					minBuildTime = unitCount.second;
				}
			}
		}
		if (minBuildTime > minTime)
		{
			minTime = minBuildTime;
		}
	}
	
	// check how long until there is a unit available to build
	UnitType buildType = action.unit.whatBuilds().first;
	int availableToBuild = unitCounts[buildType];
	int minBuildWaitTime = 0;
	for (auto &busyCount : unitBusyTimes)
	{
		if (busyCount.first == buildType)
		{
			availableToBuild -= 1;
			if (minBuildWaitTime == 0)
			{
				minBuildWaitTime = busyCount.second;
			}
			else
			{
				minBuildWaitTime = std::min(minBuildWaitTime, busyCount.second);
			}
		}
	}
	for (auto &unitCount : unitsInProgressCounts)
	{
		if (unitCount.first == buildType)
		{
			if (minBuildWaitTime == 0)
			{
				minBuildWaitTime = unitCount.second;
			}
			else
			{
				minBuildWaitTime = std::min(minBuildWaitTime, unitCount.second);
			}
		}
	}
	if (availableToBuild <= 0)
	{
		minTime = std::max(minTime, minBuildWaitTime);
	}

	// advance time by minTime
	gameTime += minTime;
	minerals += mineralWorkers * 0.045 * minTime;
	gas += gasWorkers * 0.07 * minTime;

	// iterate over in-progress units, upgrades, and techs to advance them by minTime
	std::vector<std::pair<UnitType, int>>::iterator unitIter;
	for (unitIter = unitsInProgressCounts.begin(); unitIter != unitsInProgressCounts.end();)
	{
		if (unitIter->second > minTime)
		{
			unitIter->second -= minTime;
			++unitIter;
		}
		else
		{
			// if unit has been completed, update unit counts
			unitCounts[unitIter->first] += 1;
			// if unit is a worker, assign them to minerals or gas, in accordance with some rule
			if (unitIter->first.isWorker())
			{
				if (mineralWorkers <= 8 || gasWorkers >= 3 || Broodwar->self()->completedUnitCount(UnitTypes::Protoss_Assimilator) == 0)
				{
					mineralWorkers += 1;
					minerals += 0.045 * (minTime - unitIter->second);
				}
				else
				{
					gasWorkers += 1;
					gas += 0.07 * (minTime - unitIter->second);
				}
			}
			// if unit is nexus or pylon, update supply
			if (unitIter->first == UnitTypes::Protoss_Nexus || unitIter->first == UnitTypes::Protoss_Pylon)
				supplyTotal += unitIter->first.supplyProvided();
			unitIter = unitsInProgressCounts.erase(unitIter);
		}
	}

	std::map<UpgradeType, int>::iterator upgradeIter = upgradesInProgressCounts.begin();
	while (upgradeIter != upgradesInProgressCounts.end())
	{
		if (upgradeIter->second > minTime)
		{
			upgradeIter->second -= minTime;
			++upgradeIter;
		}
		else
		{
			upgradeIter = upgradesInProgressCounts.erase(upgradeIter);
		}
	}

	std::map<TechType, int>::iterator techIter = techsInProgressCounts.begin();
	while (techIter != techsInProgressCounts.end())
	{
		if (techIter->second > minTime)
		{
			techIter->second -= minTime;
			++techIter;
		}
		else
		{
			techIter = techsInProgressCounts.erase(techIter);
		}
	}

	// iterate over busy units and make them not busy
	std::vector<std::pair<UnitType, int>>::iterator busyIter;
	for (busyIter = unitBusyTimes.begin(); busyIter != unitBusyTimes.end();)
	{
		if (busyIter->second > minTime)
		{
			busyIter->second -= minTime;
			++busyIter;
		}
		else
		{
			// if unit is no longer busy, update busy times
			busyIter = unitBusyTimes.erase(busyIter);
		}
	}

	// if we need to devote a non-worker builder, remove them from their current job
	if (action.builder && !action.builder.isWorker())
	{
		std::pair<BWAPI::UnitType, int> unitTime(action.builder, action.timeCost);
		unitBusyTimes.push_back(unitTime);
	}
	// mark whatever is being built as in-progress
	if (action.unit != UnitTypes::None)
	{
		std::pair<BWAPI::UnitType, int> unitTime(action.unit, action.timeCost);
		unitsInProgressCounts.push_back(unitTime);
		if (goalUnits.count(action.unit) > 0)
		{
			goalUnits[action.unit] -= 1;
			if (goalUnits[action.unit] <= 0)
			{
				goalUnits.erase(action.unit);
			}
		}
	}
	else if (action.upgrade != UpgradeTypes::None)
	{
		upgradesInProgressCounts[action.upgrade] = action.timeCost;
		if (goalUpgrades.count(action.upgrade) > 0)
		{
			goalUpgrades[action.upgrade] -= 1;
			if (goalUpgrades[action.upgrade] <= 0)
			{
				goalUpgrades.erase(action.upgrade);
			}
		}
	}
	else if (action.tech != TechTypes::None)
	{
		techsInProgressCounts[action.tech] = action.timeCost;
		if (goalTechs.count(action.tech) > 0)
		{
			goalTechs.erase(action.tech);
		}
	}
}

/* Constructor to transition to new game state via delay */
MacroGameState::MacroGameState(const MacroGameState &currentState, int delay)
{
	gameTime = currentState.gameTime + delay;
	minerals = currentState.minerals + 0.045 * delay * currentState.mineralWorkers;
	gas = currentState.gas + 0.07 * delay * currentState.gasWorkers;
	supplyUsed = currentState.supplyUsed;
	supplyTotal = currentState.supplyTotal;
	mineralWorkers = currentState.mineralWorkers;
	gasWorkers = currentState.gasWorkers;
	unitCounts = std::map<BWAPI::UnitType, int>(currentState.unitCounts);
	unitsInProgressCounts = std::vector<std::pair<BWAPI::UnitType, int>>(currentState.unitsInProgressCounts);
	upgradesInProgressCounts = std::map<BWAPI::UpgradeType, int>(currentState.upgradesInProgressCounts);
	techsInProgressCounts = std::map<BWAPI::TechType, int>(currentState.techsInProgressCounts);
	unitBusyTimes = std::vector<std::pair<BWAPI::UnitType, int>>(currentState.unitBusyTimes);
	goalUnits = std::map<BWAPI::UnitType, int>(currentState.goalUnits);
	goalUpgrades = std::map<BWAPI::UpgradeType, int>(currentState.goalUpgrades);
	goalTechs = std::set<BWAPI::TechType>(currentState.goalTechs);

	// iterate over in-progress units, upgrades, and techs to advance them by delay
	std::vector<std::pair<UnitType, int>>::iterator unitIter;
	for (unitIter = unitsInProgressCounts.begin(); unitIter != unitsInProgressCounts.end();)
	{
		if (unitIter->second > delay)
		{
			unitIter->second -= delay;
			++unitIter;
		}
		else
		{
			// if unit has been completed, update unit counts
			unitCounts[unitIter->first] += 1;
			// if unit is a worker, assign them to minerals or gas, in accordance with some rule
			if (unitIter->first.isWorker())
			{
				if (mineralWorkers <= 8 || gasWorkers >= 3 || Broodwar->self()->completedUnitCount(UnitTypes::Protoss_Assimilator) == 0)
				{
					mineralWorkers += 1;
					minerals += 0.045 * (delay - unitIter->second);
				}
				else
				{
					gasWorkers += 1;
					gas += 0.07 * (delay - unitIter->second);
				}
			}
			// if unit is nexus or pylon, update supply
			if (unitIter->first == UnitTypes::Protoss_Nexus || unitIter->first == UnitTypes::Protoss_Pylon)
				supplyTotal += unitIter->first.supplyProvided();
			unitIter = unitsInProgressCounts.erase(unitIter);
		}
	}

	std::map<UpgradeType, int>::iterator upgradeIter = upgradesInProgressCounts.begin();
	while (upgradeIter != upgradesInProgressCounts.end())
	{
		if (upgradeIter->second > delay)
		{
			upgradeIter->second -= delay;
			++upgradeIter;
		}
		else
		{
			upgradeIter = upgradesInProgressCounts.erase(upgradeIter);
		}
	}

	std::map<TechType, int>::iterator techIter = techsInProgressCounts.begin();
	while (techIter != techsInProgressCounts.end())
	{
		if (techIter->second > delay)
		{
			techIter->second -= delay;
			++techIter;
		}
		else
		{
			techIter = techsInProgressCounts.erase(techIter);
		}
	}

	// iterate over busy units and make them not busy
	std::vector<std::pair<UnitType, int>>::iterator busyIter;
	for (busyIter = unitBusyTimes.begin(); busyIter != unitBusyTimes.end();)
	{
		if (busyIter->second > delay)
		{
			busyIter->second -= delay;
			++busyIter;
		}
		else
		{
			// if unit is no longer busy, update busy times
			busyIter = unitBusyTimes.erase(busyIter);
		}
	}
}


bool MacroGameState::isGoalState()
{
	// check if all parts of goal have been satisfied
	if (goalUnits.empty() && goalUpgrades.empty() && goalTechs.empty())
	{
		return true;
	}
	return false;
}

std::vector<BuildOrderAction> MacroGameState::getTwoGateZealotRushActions()
{
	std::vector<BuildOrderAction> result;
	if (hasBuildPotential(UnitTypes::Protoss_Probe))
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_Probe));
	}
	if (hasBuildPotential(UnitTypes::Protoss_Pylon))
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_Pylon));
	}
	if (hasBuildPotential(UnitTypes::Protoss_Zealot))
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_Zealot));
	}
	if (hasBuildPotential(UnitTypes::Protoss_Gateway))
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_Gateway));
	}
	return result;
}

std::vector<BuildOrderAction> MacroGameState::getDTDropActions()
{
	std::vector<BuildOrderAction> result;
	if (goalUnits.count(UnitTypes::Protoss_Zealot))
	{
		if (hasBuildPotential(UnitTypes::Protoss_Probe))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Probe));
		}
		if (hasBuildPotential(UnitTypes::Protoss_Zealot))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Zealot));
		}
		if (hasBuildPotential(UnitTypes::Protoss_Pylon))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Pylon));
		}
		if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Cybernetics_Core) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Cybernetics_Core) < 1 && hasBuildPotential(UnitTypes::Protoss_Cybernetics_Core))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Cybernetics_Core));
		}
		if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Gateway) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Gateway) < 1 && hasBuildPotential(UnitTypes::Protoss_Gateway))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Gateway));
		}
		if (hasBuildPotential(UnitTypes::Protoss_Citadel_of_Adun) && Broodwar->self()->allUnitCount(UnitTypes::Protoss_Citadel_of_Adun) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Citadel_of_Adun) < 1)
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Citadel_of_Adun));
		}
		if (Broodwar->self()->completedUnitCount(UnitTypes::Protoss_Templar_Archives) >= 1 && Broodwar->self()->allUnitCount(UnitTypes::Protoss_Forge) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Forge) < 1 && hasBuildPotential(UnitTypes::Protoss_Forge))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Forge));
		}
		if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Forge) >= 1 && Broodwar->self()->allUnitCount(UnitTypes::Protoss_Photon_Cannon) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Photon_Cannon) < 1 && hasBuildPotential(UnitTypes::Protoss_Photon_Cannon))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Photon_Cannon));
		}
		if (Broodwar->self()->completedUnitCount(UnitTypes::Protoss_Templar_Archives) >= 1 && Broodwar->self()->allUnitCount(UnitTypes::Protoss_Robotics_Facility) < 1 && hasBuildPotential(UnitTypes::Protoss_Robotics_Facility))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Robotics_Facility));
		}
		if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Robotics_Facility) >= 1 && Broodwar->self()->allUnitCount(UnitTypes::Protoss_Observatory) < 1 && hasBuildPotential(UnitTypes::Protoss_Observatory))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Observatory));
		}
		if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Observatory) >= 1 && Broodwar->self()->allUnitCount(UnitTypes::Protoss_Observer) < 1 && hasBuildPotential(UnitTypes::Protoss_Observer))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Observer));
		}
		if (goalUpgrades.count(UpgradeTypes::Leg_Enhancements))
		{
			Broodwar << "need LE" << std::endl;
			if (hasBuildPotential(UpgradeTypes::Leg_Enhancements))
			{
				Broodwar << "can build LE" << std::endl;
				result.push_back(BuildOrderAction(UnitTypes::None, UpgradeTypes::Leg_Enhancements));
			}
		}
		if (goalUpgrades.count(UpgradeTypes::Gravitic_Boosters))
		{
			Broodwar << "need GB" << std::endl;
			if (hasBuildPotential(UpgradeTypes::Gravitic_Boosters))
			{
				Broodwar << "can build GB" << std::endl;
				result.push_back(BuildOrderAction(UnitTypes::None, UpgradeTypes::Gravitic_Boosters));
			}
		}
		return result;
	}
	if (goalUnits.count(UnitTypes::Protoss_Dark_Templar))
	{
		if (hasBuildPotential(UnitTypes::Protoss_Probe))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Probe));
		}
		if (hasBuildPotential(UnitTypes::Protoss_Dark_Templar))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Dark_Templar));
		}
		if (hasBuildPotential(UnitTypes::Protoss_Pylon))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Pylon));
		}
		if (goalUpgrades.count(UpgradeTypes::Leg_Enhancements))
		{
			Broodwar << "need LE" << std::endl;
			if (hasBuildPotential(UpgradeTypes::Leg_Enhancements))
			{
				Broodwar << "can build LE" << std::endl;
				result.push_back(BuildOrderAction(UnitTypes::None, UpgradeTypes::Leg_Enhancements));
			}
		}
		return result;
	}
	if (goalUnits.count(UnitTypes::Protoss_Templar_Archives))
	{
		Broodwar << "GT" << std::endl;
		if (hasBuildPotential(UnitTypes::Protoss_Pylon))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Pylon));
		}
		if (hasBuildPotential(UnitTypes::Protoss_Probe))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Probe));
		}
		if (hasBuildPotential(UnitTypes::Protoss_Gateway))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Gateway));
		}
		if (hasBuildPotential(UnitTypes::Protoss_Templar_Archives))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Templar_Archives));
		}
		if (hasBuildPotential(UnitTypes::Protoss_Zealot))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Zealot));
		}
		if (goalUpgrades.count(UpgradeTypes::Leg_Enhancements))
		{
			Broodwar << "need LE" << std::endl;
			if (hasBuildPotential(UpgradeTypes::Leg_Enhancements))
			{
				Broodwar << "can build LE" << std::endl;
				result.push_back(BuildOrderAction(UnitTypes::None, UpgradeTypes::Leg_Enhancements));
			}
		}
		return result;
	}
	if (hasBuildPotential(UnitTypes::Protoss_Probe))
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_Probe));
	}
	if (hasBuildPotential(UnitTypes::Protoss_Pylon))
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_Pylon));
	}
	if (hasBuildPotential(UnitTypes::Protoss_Gateway) && (goalUnits.count(UnitTypes::Protoss_Gateway) || goalUnits.count(UnitTypes::Protoss_Dragoon) || goalUnits.count(UnitTypes::Protoss_Dark_Templar)) )
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_Gateway));
	}
	if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Assimilator) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Assimilator) < 1 && hasBuildPotential(UnitTypes::Protoss_Assimilator))
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_Assimilator));
	}
	if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Cybernetics_Core) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Cybernetics_Core) < 1 && hasBuildPotential(UnitTypes::Protoss_Cybernetics_Core))
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_Cybernetics_Core));
	}
	if (hasBuildPotential(UnitTypes::Protoss_Citadel_of_Adun) && Broodwar->self()->allUnitCount(UnitTypes::Protoss_Citadel_of_Adun) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Citadel_of_Adun) < 1)
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_Citadel_of_Adun));
	}
	if (hasBuildPotential(UnitTypes::Protoss_Templar_Archives) && Broodwar->self()->allUnitCount(UnitTypes::Protoss_Templar_Archives) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Templar_Archives) < 1)
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_Templar_Archives));
	}
	if (hasBuildPotential(UnitTypes::Protoss_Dark_Templar))
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_Dark_Templar));
	}
	if (goalUpgrades.count(UpgradeTypes::Leg_Enhancements))
	{
		Broodwar << "need LE" << std::endl;
		if (hasBuildPotential(UpgradeTypes::Leg_Enhancements))
		{
			Broodwar << "can build LE" << std::endl;
			result.push_back(BuildOrderAction(UnitTypes::None, UpgradeTypes::Leg_Enhancements));
		}
	}
	if (goalUpgrades.count(UpgradeTypes::Gravitic_Boosters))
	{
		Broodwar << "need GB" << std::endl;
		if (hasBuildPotential(UpgradeTypes::Gravitic_Boosters))
		{
			Broodwar << "can build GB" << std::endl;
			result.push_back(BuildOrderAction(UnitTypes::None, UpgradeTypes::Gravitic_Boosters));
		}
	}
	return result;
}

std::vector<BuildOrderAction> MacroGameState::getSpeedZealActions()
{
	std::vector<BuildOrderAction> result;
	if (goalUnits.count(UnitTypes::Protoss_High_Templar))
	{
		if (hasBuildPotential(UnitTypes::Protoss_Probe))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Probe));
		}
		if (hasBuildPotential(UnitTypes::Protoss_High_Templar))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_High_Templar));
		}
		if (hasBuildPotential(UnitTypes::Protoss_Pylon))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Pylon));
		}
		return result;
	}
	if (goalUnits.count(UnitTypes::Protoss_Templar_Archives))
	{
		Broodwar << "GT" << std::endl;
		if (hasBuildPotential(UnitTypes::Protoss_Pylon))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Pylon));
		}
		if (hasBuildPotential(UnitTypes::Protoss_Probe))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Probe));
		}
		if (hasBuildPotential(UnitTypes::Protoss_Templar_Archives))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Templar_Archives));
		}
		if (hasBuildPotential(UnitTypes::Protoss_Zealot))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Zealot));
		}
		return result;
	}
	if (hasBuildPotential(UnitTypes::Protoss_Probe))
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_Probe));
	}
	if (hasBuildPotential(UnitTypes::Protoss_Pylon))
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_Pylon));
	}
	if (hasBuildPotential(UnitTypes::Protoss_Gateway) && goalUnits.count(UnitTypes::Protoss_Gateway))
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_Gateway));
	}
	if (hasBuildPotential(UnitTypes::Protoss_Zealot))
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_Zealot));
	}
	if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Assimilator) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Assimilator) < 1 && hasBuildPotential(UnitTypes::Protoss_Assimilator))
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_Assimilator));
	}
	if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Cybernetics_Core) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Cybernetics_Core) < 1 && hasBuildPotential(UnitTypes::Protoss_Cybernetics_Core))
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_Cybernetics_Core));
	}
	if (hasBuildPotential(UnitTypes::Protoss_Citadel_of_Adun) && Broodwar->self()->allUnitCount(UnitTypes::Protoss_Citadel_of_Adun) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Citadel_of_Adun) < 1)
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_Citadel_of_Adun));
	}
	if (hasBuildPotential(UnitTypes::Protoss_Templar_Archives) && Broodwar->self()->allUnitCount(UnitTypes::Protoss_Templar_Archives) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Templar_Archives) < 1)
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_Templar_Archives));
	}
	if (hasBuildPotential(UnitTypes::Protoss_High_Templar))
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_High_Templar));
	}
	if (goalUpgrades.count(UpgradeTypes::Leg_Enhancements))
	{
		Broodwar << "need LE" << std::endl;
		if (hasBuildPotential(UpgradeTypes::Leg_Enhancements))
		{
			Broodwar << "can build LE" << std::endl;
			result.push_back(BuildOrderAction(UnitTypes::None, UpgradeTypes::Leg_Enhancements));
		}
	}
	return result;
}

std::vector<BuildOrderAction> MacroGameState::getCorsairDarkTemplarActions()
{
	std::vector<BuildOrderAction> result;
	if (goalUnits.count(UnitTypes::Protoss_Nexus))
	{
		if (hasBuildPotential(UnitTypes::Protoss_Nexus))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Nexus));
		}
		if (hasBuildPotential(UnitTypes::Protoss_Probe))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Probe));
		}
	}
	if (goalUnits.count(UnitTypes::Protoss_Zealot))
	{
		if (hasBuildPotential(UnitTypes::Protoss_Probe))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Probe));
		}
		if (hasBuildPotential(UnitTypes::Protoss_Zealot))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Zealot));
		}
		if (hasBuildPotential(UnitTypes::Protoss_Pylon))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Pylon));
		}
		if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Cybernetics_Core) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Cybernetics_Core) < 1 && hasBuildPotential(UnitTypes::Protoss_Cybernetics_Core))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Cybernetics_Core));
		}
		if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Gateway) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Gateway) < 1 && hasBuildPotential(UnitTypes::Protoss_Gateway))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Gateway));
		}
		if (hasBuildPotential(UnitTypes::Protoss_Citadel_of_Adun) && Broodwar->self()->allUnitCount(UnitTypes::Protoss_Citadel_of_Adun) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Citadel_of_Adun) < 1)
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Citadel_of_Adun));
		}
		if (Broodwar->self()->completedUnitCount(UnitTypes::Protoss_Templar_Archives) >= 1 && Broodwar->self()->allUnitCount(UnitTypes::Protoss_Stargate) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Stargate) < 1 && hasBuildPotential(UnitTypes::Protoss_Stargate))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Stargate));
		}
		if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Stargate) >= 1 && Broodwar->self()->allUnitCount(UnitTypes::Protoss_Corsair) < 1 && hasBuildPotential(UnitTypes::Protoss_Corsair))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Corsair));
		}
		if (Broodwar->self()->completedUnitCount(UnitTypes::Protoss_Templar_Archives) >= 1 && Broodwar->self()->allUnitCount(UnitTypes::Protoss_Robotics_Facility) < 1 && hasBuildPotential(UnitTypes::Protoss_Robotics_Facility))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Robotics_Facility));
		}
		if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Robotics_Facility) >= 1 && Broodwar->self()->allUnitCount(UnitTypes::Protoss_Observatory) < 1 && hasBuildPotential(UnitTypes::Protoss_Observatory))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Observatory));
		}
		if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Observatory) >= 1 && Broodwar->self()->allUnitCount(UnitTypes::Protoss_Observer) < 1 && hasBuildPotential(UnitTypes::Protoss_Observer))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Observer));
		}
		if (goalUpgrades.count(UpgradeTypes::Leg_Enhancements))
		{
			Broodwar << "need LE" << std::endl;
			if (hasBuildPotential(UpgradeTypes::Leg_Enhancements))
			{
				Broodwar << "can build LE" << std::endl;
				result.push_back(BuildOrderAction(UnitTypes::None, UpgradeTypes::Leg_Enhancements));
			}
		}
		if (goalUpgrades.count(UpgradeTypes::Gravitic_Boosters))
		{
			Broodwar << "need GB" << std::endl;
			if (hasBuildPotential(UpgradeTypes::Gravitic_Boosters))
			{
				Broodwar << "can build GB" << std::endl;
				result.push_back(BuildOrderAction(UnitTypes::None, UpgradeTypes::Gravitic_Boosters));
			}
		}
		return result;
	}
	if (goalUnits.count(UnitTypes::Protoss_Dark_Templar))
	{
		if (hasBuildPotential(UnitTypes::Protoss_Probe))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Probe));
		}
		if (hasBuildPotential(UnitTypes::Protoss_Dark_Templar))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Dark_Templar));
		}
		if (hasBuildPotential(UnitTypes::Protoss_Pylon))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Pylon));
		}
		if (goalUpgrades.count(UpgradeTypes::Leg_Enhancements))
		{
			Broodwar << "need LE" << std::endl;
			if (hasBuildPotential(UpgradeTypes::Leg_Enhancements))
			{
				Broodwar << "can build LE" << std::endl;
				result.push_back(BuildOrderAction(UnitTypes::None, UpgradeTypes::Leg_Enhancements));
			}
		}
		return result;
	}
	if (goalUnits.count(UnitTypes::Protoss_Templar_Archives))
	{
		Broodwar << "GT" << std::endl;
		if (hasBuildPotential(UnitTypes::Protoss_Pylon))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Pylon));
		}
		if (hasBuildPotential(UnitTypes::Protoss_Probe))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Probe));
		}
		if (hasBuildPotential(UnitTypes::Protoss_Gateway))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Gateway));
		}
		if (hasBuildPotential(UnitTypes::Protoss_Templar_Archives))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Templar_Archives));
		}
		if (hasBuildPotential(UnitTypes::Protoss_Zealot))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Zealot));
		}
		if (goalUpgrades.count(UpgradeTypes::Leg_Enhancements))
		{
			Broodwar << "need LE" << std::endl;
			if (hasBuildPotential(UpgradeTypes::Leg_Enhancements))
			{
				Broodwar << "can build LE" << std::endl;
				result.push_back(BuildOrderAction(UnitTypes::None, UpgradeTypes::Leg_Enhancements));
			}
		}
		return result;
	}
	if (hasBuildPotential(UnitTypes::Protoss_Probe))
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_Probe));
	}
	if (hasBuildPotential(UnitTypes::Protoss_Pylon))
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_Pylon));
	}
	if (hasBuildPotential(UnitTypes::Protoss_Gateway) && (goalUnits.count(UnitTypes::Protoss_Gateway) || goalUnits.count(UnitTypes::Protoss_Dragoon) || goalUnits.count(UnitTypes::Protoss_Dark_Templar)))
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_Gateway));
	}
	if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Assimilator) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Assimilator) < 1 && hasBuildPotential(UnitTypes::Protoss_Assimilator))
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_Assimilator));
	}
	if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Cybernetics_Core) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Cybernetics_Core) < 1 && hasBuildPotential(UnitTypes::Protoss_Cybernetics_Core))
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_Cybernetics_Core));
	}
	if (hasBuildPotential(UnitTypes::Protoss_Citadel_of_Adun) && Broodwar->self()->allUnitCount(UnitTypes::Protoss_Citadel_of_Adun) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Citadel_of_Adun) < 1)
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_Citadel_of_Adun));
	}
	if (hasBuildPotential(UnitTypes::Protoss_Templar_Archives) && Broodwar->self()->allUnitCount(UnitTypes::Protoss_Templar_Archives) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Templar_Archives) < 1)
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_Templar_Archives));
	}
	if (hasBuildPotential(UnitTypes::Protoss_Dark_Templar))
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_Dark_Templar));
	}
	if (goalUpgrades.count(UpgradeTypes::Leg_Enhancements))
	{
		Broodwar << "need LE" << std::endl;
		if (hasBuildPotential(UpgradeTypes::Leg_Enhancements))
		{
			Broodwar << "can build LE" << std::endl;
			result.push_back(BuildOrderAction(UnitTypes::None, UpgradeTypes::Leg_Enhancements));
		}
	}
	if (goalUpgrades.count(UpgradeTypes::Gravitic_Boosters))
	{
		Broodwar << "need GB" << std::endl;
		if (hasBuildPotential(UpgradeTypes::Gravitic_Boosters))
		{
			Broodwar << "can build GB" << std::endl;
			result.push_back(BuildOrderAction(UnitTypes::None, UpgradeTypes::Gravitic_Boosters));
		}
	}
	return result;
}

std::vector<BuildOrderAction> MacroGameState::getThreeGateRoboActions()
{
	std::vector<BuildOrderAction> result;
	if (goalUnits.count(UnitTypes::Protoss_Dragoon))
	{
		if (hasBuildPotential(UnitTypes::Protoss_Probe))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Probe));
		}
		if (hasBuildPotential(UnitTypes::Protoss_Dragoon))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Dragoon));
		}
		if (hasBuildPotential(UnitTypes::Protoss_Observer))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Observer));
		}
		if (hasBuildPotential(UnitTypes::Protoss_Pylon))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Pylon));
		}
		return result;
	}
	if (goalUnits.count(UnitTypes::Protoss_Zealot))
	{
		if (hasBuildPotential(UnitTypes::Protoss_Probe))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Probe));
		}
		if (hasBuildPotential(UnitTypes::Protoss_Zealot))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Zealot));
		}
		if (hasBuildPotential(UnitTypes::Protoss_Pylon))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Pylon));
		}
		return result;
	}
	if (goalUpgrades.count(UpgradeTypes::Singularity_Charge))
	{
		Broodwar << "need singularity charge" << std::endl;
		if (hasBuildPotential(UpgradeTypes::Singularity_Charge))
		{	
			Broodwar << "can build singularity charge" << std::endl;
			result.push_back(BuildOrderAction(UnitTypes::None, UpgradeTypes::Singularity_Charge));
		}
		return result;
	}
	if (hasBuildPotential(UnitTypes::Protoss_Probe))
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_Probe));
	}
	if (hasBuildPotential(UnitTypes::Protoss_Pylon))
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_Pylon));
	}
	if (hasBuildPotential(UnitTypes::Protoss_Gateway) && (goalUnits.count(UnitTypes::Protoss_Gateway) || goalUnits.count(UnitTypes::Protoss_Dragoon) || goalUnits.count(UnitTypes::Protoss_Dark_Templar)))
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_Gateway));
	}
	if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Assimilator) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Assimilator) < 1 && hasBuildPotential(UnitTypes::Protoss_Assimilator))
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_Assimilator));
	}
	if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Cybernetics_Core) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Cybernetics_Core) < 1 && hasBuildPotential(UnitTypes::Protoss_Cybernetics_Core))
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_Cybernetics_Core));
	}
	if (hasBuildPotential(UnitTypes::Protoss_Robotics_Facility) && Broodwar->self()->allUnitCount(UnitTypes::Protoss_Robotics_Facility) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Robotics_Facility) < 1)
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_Robotics_Facility));
	}
	if (hasBuildPotential(UnitTypes::Protoss_Observatory) && Broodwar->self()->allUnitCount(UnitTypes::Protoss_Observatory) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Observatory) < 1)
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_Observatory));
	}
	return result;
}

/* get a vector of feasible actions for this game state */
std::vector<BuildOrderAction> MacroGameState::getFeasibleActions()
{
	if (StrategyManager::getInstance().getStrategy() == "TwoGateZealotRush")
	{
		return getTwoGateZealotRushActions();
	}
	if (StrategyManager::getInstance().getStrategy() == "DTDrop")
	{
		return getDTDropActions();
	}
	if (StrategyManager::getInstance().getStrategy() == "ThreeGateRobo")
	{
		return getThreeGateRoboActions();
	}
	if (StrategyManager::getInstance().getStrategy() == "CorsairDarkTemplar")
	{
		return getCorsairDarkTemplarActions();
	}
	if (StrategyManager::getInstance().getStrategy() == "SpeedZeal")
	{
		return getSpeedZealActions();
	}
	std::vector<BuildOrderAction> result;
	if (goalUnits.count(UnitTypes::Protoss_Dragoon) > 0)
	{
		if (hasBuildPotential(UnitTypes::Protoss_Probe))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Probe));
		}
		if (hasBuildPotential(UnitTypes::Protoss_Pylon))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Pylon));
		}
		if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Assimilator) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Assimilator) < 1 && hasBuildPotential(UnitTypes::Protoss_Assimilator))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Assimilator));
		}
		if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Cybernetics_Core) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Cybernetics_Core) < 1 && hasBuildPotential(UnitTypes::Protoss_Cybernetics_Core))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Cybernetics_Core));
		}
		if (hasBuildPotential(UnitTypes::Protoss_Dragoon))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Dragoon));
		}
		return result;
	}
	if (goalUnits.count(UnitTypes::Protoss_Shuttle) > 0)
	{
		Broodwar << "need Shuttle" << std::endl;
		if (hasBuildPotential(UnitTypes::Protoss_Probe))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Probe));
		}
		if (hasBuildPotential(UnitTypes::Protoss_Pylon))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Pylon));
		}
		if (hasBuildPotential(UnitTypes::Protoss_Shuttle))
		{
			Broodwar << "can build Shuttle" << std::endl;
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Shuttle));
		}
		return result;
	}
	if (goalUnits.count(UnitTypes::Protoss_Dark_Templar) > 0)
	{
		Broodwar << "need Dark Templar" << std::endl;
		if (hasBuildPotential(UnitTypes::Protoss_Probe))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Probe));
		}
		if (hasBuildPotential(UnitTypes::Protoss_Pylon))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Pylon));
		}
		if (hasBuildPotential(UnitTypes::Protoss_Dark_Templar))
		{
			Broodwar << "can build Dark Templar" << std::endl;
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Dark_Templar));
		}
		return result;
	}
	if (goalUnits.count(UnitTypes::Protoss_Citadel_of_Adun) > 0)
	{
		Broodwar << "need Citadel" << std::endl;
		if (hasBuildPotential(UnitTypes::Protoss_Probe))
		{
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Probe));
		}
		if (hasBuildPotential(UnitTypes::Protoss_Citadel_of_Adun))
		{
			Broodwar << "can build Citadel" << std::endl;
			result.push_back(BuildOrderAction(UnitTypes::Protoss_Citadel_of_Adun));
		}
		return result;
	}
	// for now, we'll assume we only ever build a handful of things:
	// probe, pylon, nexus, assimilator, gateway, zealot, dragoon
	if (hasBuildPotential(UnitTypes::Protoss_Probe))
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_Probe));
	}
	if (hasBuildPotential(UnitTypes::Protoss_Pylon))
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_Pylon));
	}
	/*
	if (hasBuildPotential(UnitTypes::Protoss_Nexus))
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_Nexus));
	}
	*/
	if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Assimilator) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Assimilator) < 1 && hasBuildPotential(UnitTypes::Protoss_Assimilator))
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_Assimilator));
	}
	
	if (hasBuildPotential(UnitTypes::Protoss_Gateway))
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_Gateway));
	}
	if (hasBuildPotential(UnitTypes::Protoss_Zealot))
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_Zealot));
	}
	if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Cybernetics_Core) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Cybernetics_Core) < 1 && hasBuildPotential(UnitTypes::Protoss_Cybernetics_Core))
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_Cybernetics_Core));
	}
	if (hasBuildPotential(UnitTypes::Protoss_Dragoon))
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_Dragoon));
	}
	if (hasBuildPotential(UnitTypes::Protoss_Citadel_of_Adun))
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_Citadel_of_Adun));
	}
	if (hasBuildPotential(UnitTypes::Protoss_Robotics_Facility))
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_Robotics_Facility));
	}
	if (hasBuildPotential(UnitTypes::Protoss_Templar_Archives))
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_Templar_Archives));
	}
	if (hasBuildPotential(UnitTypes::Protoss_Shuttle))
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_Shuttle));
	}
	if (hasBuildPotential(UnitTypes::Protoss_Dark_Templar))
	{
		result.push_back(BuildOrderAction(UnitTypes::Protoss_Dark_Templar));
	}
	return result;
}

bool MacroGameState::hasBuildPotential(UnitType unit)
{
	// check for insufficient resource production
	if (unit.mineralPrice() > minerals && mineralWorkers == 0)
	{
		/*
		std::ofstream ofs;
		ofs.open("C:\\Users\\suhsa\\Source\\Repos\\starcraft\\BWAPI\\Release\\debug.txt", std::ofstream::out | std::ofstream::app);
		ofs << "    ";
		ofs << unit << " unavailable: not enough minerals";
		ofs << "\n";
		ofs.close();
		*/
		return false;
	}
	if (unit.gasPrice() > gas && gasWorkers == 0)
	{
		return false;
	}
	int supplyComing = 0;
	for (auto &unitCount : unitsInProgressCounts)
	{
		supplyComing += unitCount.first.supplyProvided();
	}
	if (unit.supplyRequired() > supplyTotal - supplyUsed + supplyComing && unit.supplyRequired() > 0)
	{
		/*
		std::ofstream ofs;
		ofs.open("C:\\Users\\suhsa\\Source\\Repos\\starcraft\\BWAPI\\Release\\debug.txt", std::ofstream::out | std::ofstream::app);
		ofs << "    ";
		ofs << unit << " unavailable: not enough supply: required: " << unit.supplyRequired() << " total: " << supplyTotal << " used: " << supplyUsed << " coming: " << supplyComing;
		ofs << "\n";
		ofs.close();
		*/
		return false;
	}
	// check for required units
	std::map<BWAPI::UnitType, int> requiredUnits = unit.requiredUnits();
	// most buildings need pylons
	if (unit.isBuilding() && !(unit == UnitTypes::Protoss_Pylon || unit == UnitTypes::Protoss_Nexus || unit == UnitTypes::Protoss_Assimilator))
	{
		requiredUnits[UnitTypes::Protoss_Pylon] = 1;
	}
	for (auto &unitCount : unitCounts)
	{
		// if we've satisfied all requirements, stop checking
		if (requiredUnits.empty())
		{
			break;
		}
		// remove units we have from required units
		if (requiredUnits.count(unitCount.first) > 0)
		{
			if (unitCount.second >= requiredUnits[unitCount.first])
			{
				requiredUnits.erase(unitCount.first);
			}
			else
			{
				requiredUnits[unitCount.first] -= unitCount.second;
			}
		}
	}
	// if we are still building required units, see if all required units are in progress
	if (!requiredUnits.empty())
	{
		for (auto &unitCount : unitsInProgressCounts)
		{
			if (requiredUnits.count(unitCount.first))
			{
				requiredUnits[unitCount.first] -= 1;
				if (requiredUnits[unitCount.first] <= 0)
				{
					requiredUnits.erase(unitCount.first);
					if (requiredUnits.empty())
					{
						break;
					}
				}
			}
		}
		if (!requiredUnits.empty())
		{
			for (auto &unitCount : ResourceManager::getInstance().unitsInProgressCounts)
			{
				if (requiredUnits.count(unitCount.first))
				{
					requiredUnits[unitCount.first] -= unitCount.second.first;
					if (requiredUnits[unitCount.first] <= 0)
					{
						requiredUnits.erase(unitCount.first);
						if (requiredUnits.empty())
						{
							break;
						}
					}
				}
			}
		}
	}
	if (!requiredUnits.empty())
	{
		/*
		std::ofstream ofs;
		ofs.open("C:\\Users\\suhsa\\Source\\Repos\\starcraft\\BWAPI\\Release\\debug.txt", std::ofstream::out | std::ofstream::app);
		ofs << "    ";
		ofs << unit << " unavailable: required units not built";
		ofs << "\n";
		ofs.close();
		*/
		return false;
	}
	return true;
}

bool MacroGameState::hasBuildPotential(UpgradeType upgrade)
{
	// check for insufficient resource production
	if (upgrade.mineralPrice() > minerals && mineralWorkers == 0)
	{
		return false;
	}
	if (upgrade.gasPrice() > gas && gasWorkers == 0)
	{
		return false;
	}
	// check for required units
	std::map<BWAPI::UnitType, int> requiredUnits;
	if (upgrade.whatsRequired() != UnitTypes::None)
	{
		requiredUnits[upgrade.whatsRequired()] = 1;
	}
	if (upgrade.whatUpgrades() != UnitTypes::None)
	{
		requiredUnits[upgrade.whatUpgrades()] = 1;
	}
	for (auto &unitCount : unitCounts)
	{
		// if we've satisfied all requirements, stop checking
		if (requiredUnits.empty())
		{
			break;
		}
		// remove units we have from required units
		if (requiredUnits.count(unitCount.first) > 0)
		{
			if (unitCount.second >= requiredUnits[unitCount.first])
			{
				requiredUnits.erase(unitCount.first);
			}
			else
			{
				requiredUnits[unitCount.first] -= unitCount.second;
			}
		}
	}
	// if we are still building required units, see if all required units are in progress
	if (!requiredUnits.empty())
	{
		for (auto &unitCount : unitsInProgressCounts)
		{
			if (requiredUnits.count(unitCount.first))
			{
				requiredUnits[unitCount.first] -= 1;
				if (requiredUnits[unitCount.first] <= 0)
				{
					requiredUnits.erase(unitCount.first);
					if (requiredUnits.empty())
					{
						break;
					}
				}
			}
		}
		if (!requiredUnits.empty())
		{
			for (auto &unitCount : ResourceManager::getInstance().unitsInProgressCounts)
			{
				if (requiredUnits.count(unitCount.first))
				{
					requiredUnits[unitCount.first] -= unitCount.second.first;
					if (requiredUnits[unitCount.first] <= 0)
					{
						requiredUnits.erase(unitCount.first);
						if (requiredUnits.empty())
						{
							break;
						}
					}
				}
			}
		}
	}
	if (!requiredUnits.empty())
	{
		for (auto & required : requiredUnits)
		{
			Broodwar << required.first << std::endl;
		}
		return false;
	}
	return true;
}

int MacroGameState::getResourceTime(int totalMineralsRequired, int totalGasRequired) {
	int minTime = 1000000000;
	
	if (gameTime < 0) {
		return 0;
	}

	if ((mineralWorkers == 0 && totalMineralsRequired > 0) || (gasWorkers == 0 && totalGasRequired > 0)) {
		return minTime;
	}

	// priority queue for fringe
	std::priority_queue<BuildOrderNode, std::vector<BuildOrderNode>, NodeComparison> fringe;

	// keep track of states visited
	std::unordered_map<MacroGameState, int, MacroGameStateHasher> visited;
	/*
	std::ofstream ofs;
	ofs.open("C:\\Users\\suhsa\\Source\\Repos\\starcraft\\BWAPI\\Release\\debug.txt", std::ofstream::out | std::ofstream::app);
	ofs << "start";
	ofs << " " << totalMineralsRequired;
	ofs << " " << minerals;
	ofs << " " << mineralWorkers;
	ofs << "\n";
	*/
	// put start state on fringe
	std::deque<BuildOrderAction> startActions;
	BuildOrderNode startNode(0, 0, startActions, *this);
	fringe.push(startNode);
	int count = 0;
	int newMinTime;
	// keep searching until we run out of nodes
	while (!fringe.empty())
	{
		// if search goes on too long, stop it
		++count;
		BuildOrderNode node = fringe.top();
		fringe.pop();
		/*
		ofs.open("C:\\Users\\suhsa\\Source\\Repos\\starcraft\\BWAPI\\Release\\debug.txt", std::ofstream::out | std::ofstream::app);
		ofs << count;
		ofs << " " << node.timeCost;
		ofs << " " << node.state.minerals;
		for (auto &action : node.actions) {
			ofs << " " << action.unit;
		}
		ofs << "\n";
		ofs.close();
		*/
		// if this state is a goal, return the actions that got us here
		// also, if fringe is too large, return actions that got us closest
		if (node.state.minerals >= totalMineralsRequired && node.state.gas >= totalGasRequired)
		{
			return std::ceil(node.timeCost - std::min((node.state.minerals - totalMineralsRequired) / (0.045 * node.state.mineralWorkers), (node.state.gas - totalGasRequired) / (0.07 * node.state.mineralWorkers)));
		}
		// if we've already visited this state, and our previous path had a lower cost, move on
		if (visited.count(node.state) > 0) {
			if (node.timeCost >= visited[node.state]) {
				continue;
			}
		}
		/*
		bool moveOn = false;
		for (auto it = visited.begin(); it != visited.end();)
		{
			if (it->first == node.state && it->second <= node.timeCost)
			{
				moveOn = true;
				break;
			}
			else if (it->first == node.state) {
				it = visited.erase(it);
			}
			else {
				++it;
			}
		}
		if (moveOn) {
			continue;
		}
		*/
		// otherwise, add the state to visited states
		visited[node.state] = node.timeCost;
		// expand the state and move on
		std::vector<BuildOrderAction> nextActions = node.state.getFeasibleActions();
		for (auto &action : nextActions)
		{
			if (action.unit != UnitTypes::Protoss_Probe && action.unit != UnitTypes::Protoss_Pylon) {
				continue;
			}

			std::deque<BuildOrderAction> previousActions = node.actions;

			// macro: workers are made at least 2 at a time
			if (previousActions.size() > 1 && previousActions.back().unit == UnitTypes::Protoss_Probe && action.unit != UnitTypes::Protoss_Probe && previousActions.end()[-2].unit != UnitTypes::Protoss_Probe) {
				continue;
			}

			previousActions.push_back(action);
			// if a list of actions involves building a ridiculous number of pylons, 
			// there's no way it's optimal
			int pylonCount = 0;
			for (auto &action2 : previousActions)
			{
				if (action2.unit == UnitTypes::Protoss_Pylon)
				{
					++pylonCount;
				}
			}
			if (pylonCount > 1 && supplyTotal + (pylonCount - 2) * UnitTypes::Protoss_Pylon.supplyProvided() > supplyUsed) {
				continue;
			}
			// if we could stop building anything, only collect resources, and still do worse than minTime, stop
			newMinTime = std::max((totalMineralsRequired - node.state.minerals) / (0.045 * (node.state.mineralWorkers + 1)), (totalGasRequired - node.state.gas) / (0.07 * (node.state.gasWorkers + 1)));
			if (minTime <= newMinTime) {
				continue;
			}

			MacroGameState nextState(node.state, action);
			BuildOrderNode nextNode(node.timeCost + nextState.gameTime - node.state.gameTime, 0, previousActions, nextState);
			fringe.push(nextNode);
		}
		// also try just doing nothing except collecting
		if (!node.state.unitsInProgressCounts.empty()) {
			/*
			newMinTime = std::max((totalMineralsRequired - node.state.minerals) / (0.045 * (node.state.mineralWorkers + 1)), (totalGasRequired - node.state.gas) / (0.07 * (node.state.gasWorkers + 1)));
			if (minTime <= newMinTime) {
				continue;
			}
			int delay = 1000000000;
			for (auto &progress : node.state.unitsInProgressCounts) {
				if (progress.first == UnitTypes::Protoss_Probe || progress.first == UnitTypes::Protoss_Pylon) {
					delay = std::min(delay, progress.second);
				}
			}
			if (delay > 0 && delay < 1000000000) {
				MacroGameState nextState(node.state, delay);
				BuildOrderNode nextNode(node.timeCost + nextState.gameTime - node.state.gameTime, 0, node.actions, nextState);
				fringe.push(nextNode);
				std::ofstream ofs;
				ofs.open("C:\\Users\\suhsa\\Source\\Repos\\starcraft\\BWAPI\\Release\\debug.txt", std::ofstream::out | std::ofstream::app);
				ofs << "delay: " << delay;
				ofs.close();
			}
			else {
				std::ofstream ofs;
				ofs.open("C:\\Users\\suhsa\\Source\\Repos\\starcraft\\BWAPI\\Release\\debug.txt", std::ofstream::out | std::ofstream::app);
				ofs << "ASDF";
				ofs.close();
			}
			*/
		}
		int delay = std::ceil(std::max((totalMineralsRequired - node.state.minerals) / (0.045 * (node.state.mineralWorkers)), (totalGasRequired - node.state.gas) / (0.07 * (node.state.gasWorkers))));
		if (delay > 0 && node.timeCost + delay < minTime) {
			MacroGameState nextState(node.state, delay);
			BuildOrderNode nextNode(node.timeCost + nextState.gameTime - node.state.gameTime, 0, node.actions, nextState);
			fringe.push(nextNode);
			if (node.state.unitsInProgressCounts.empty()) {
				minTime = std::min(minTime, nextNode.timeCost);
			}
		}
	}
	/*
	ofs.open("C:\\Users\\suhsa\\Source\\Repos\\starcraft\\BWAPI\\Release\\debug.txt", std::ofstream::out | std::ofstream::app);
	ofs << "YUIO";
	ofs.close();
	*/
	return minTime;
}

int MacroGameState::buildTimeHeuristic()
{
	int result = 0;
	int maxBuildTime = 0;
	int totalMineralsRequired = 0;
	int totalGasRequired = 0;
	int totalSupplyRequired = 0;
	/*
	std::ofstream ofs;
	ofs.open("C:\\Users\\suhsa\\Source\\Repos\\starcraft\\BWAPI\\Release\\debug.txt", std::ofstream::out | std::ofstream::app);
	ofs << "    ";
	*/
	// create a map of all units we own, in progress or not
	std::map<BWAPI::UnitType, int> owned = unitCounts;
	for (auto &unitProgress : unitsInProgressCounts) {
		owned[unitProgress.first] += 1;
	}
	// get all unbuilt units required for goal units
	std::map<BWAPI::UnitType, int> requiredUnits;
	std::map<BWAPI::UnitType, int> requiredForGoal = goalUnits;
	while (!requiredForGoal.empty()) {
		for (auto it = requiredForGoal.cbegin(); it != requiredForGoal.cend();) {
			requiredUnits[it->first] = std::max(requiredUnits[it->first], it->second);
			// most buildings need pylons
			if (it->first.isBuilding() && !(it->first == UnitTypes::Protoss_Pylon || it->first == UnitTypes::Protoss_Nexus || it->first == UnitTypes::Protoss_Assimilator))
			{
				requiredUnits[UnitTypes::Protoss_Pylon] = 1;
			}
			if (it->first != UnitTypes::Protoss_Probe && it->first != UnitTypes::Protoss_Nexus) {
				std::map<BWAPI::UnitType, int> nextRequirements = it->first.requiredUnits();
				requiredForGoal.insert(nextRequirements.begin(), nextRequirements.end());
				requiredForGoal.erase(it);
				break;
			}
			it = requiredForGoal.erase(it);
		}
	}

	for (auto &unitCount : owned)
	{
		// if we've satisfied all requirements, stop checking
		if (requiredUnits.empty())
		{
			break;
		}
		// remove units we have from required units
		if (requiredUnits.count(unitCount.first) > 0)
		{
			if (unitCount.second >= requiredUnits[unitCount.first])
			{
				requiredUnits.erase(unitCount.first);
			}
			else
			{
				requiredUnits[unitCount.first] -= unitCount.second;
			}
		}
	}

	// add goal units
	requiredUnits.insert(goalUnits.begin(), goalUnits.end());

	// find minimum time to tech up to goal units
	for (auto &unitCount : goalUnits) {
		int minBuildTime = 0;
		std::map<BWAPI::UnitType, int> requirements = unitCount.first.requiredUnits();
		std::map<BWAPI::UnitType, int> allRequirements;
		while (!requirements.empty()) {
			for (auto it = requirements.cbegin(); it != requirements.cend();) {
				allRequirements[it->first] = it->second;

				if (it->first != UnitTypes::Protoss_Probe && it->first != UnitTypes::Protoss_Nexus) {
					std::map<BWAPI::UnitType, int> nextRequirements = it->first.requiredUnits();
					requirements.insert(nextRequirements.begin(), nextRequirements.end());
					requirements.erase(it);
					break;
				}
				it = requirements.erase(it);
			}
		}
		for (auto &unitCount2 : unitCounts)
		{
			// if we've satisfied all requirements, stop checking
			if (allRequirements.empty())
			{
				break;
			}
			// remove units we have from required units
			if (allRequirements.count(unitCount2.first) > 0)
			{
				if (unitCount2.second >= allRequirements[unitCount2.first])
				{
					allRequirements.erase(unitCount2.first);
				}
				else
				{
					allRequirements[unitCount2.first] -= unitCount2.second;
				}
			}
		}
		if (!allRequirements.empty())
		{
			for (auto &unitCount2 : unitsInProgressCounts)
			{
				if (allRequirements.count(unitCount2.first))
				{
					allRequirements[unitCount2.first] -= 1;
					minBuildTime += unitCount2.second;
					if (allRequirements[unitCount2.first] <= 0)
					{
						allRequirements.erase(unitCount2.first);
						if (allRequirements.empty())
						{
							break;
						}
					}
				}
			}
		}
		for (auto &unitCount2 : allRequirements) {
			minBuildTime += unitCount2.first.buildTime() * unitCount2.second;
			if (unitCount2.first.isBuilding()) {
				minBuildTime += 150;
			}
		}
		maxBuildTime = std::max(maxBuildTime, minBuildTime);
		//ofs << "tech up: " << minBuildTime << " ";
	}
	
	for (auto &unitCount : requiredUnits) {
		if (unitCount.first.isBuilding()) {
			continue;
		}
		int minBuildTime = 1000000000;
		int numMakersOwned = owned[unitCount.first.whatBuilds().first];
		for (int i = std::max(1, numMakersOwned); i <= unitCount.second; ++i) {
			int numPreBuilt = (i - numMakersOwned > 0) * (unitCount.first.whatBuilds().first.buildTime() + 150 * unitCount.first.whatBuilds().first.isBuilding()) / (unitCount.first.buildTime());
			int newTime = (i - numMakersOwned > 0) * (unitCount.first.whatBuilds().first.buildTime() + 150 * unitCount.first.whatBuilds().first.isBuilding()) + std::max(0, (unitCount.second - numPreBuilt - 1) / i) * (unitCount.first.buildTime());
			int totalMinerals = (i - numMakersOwned) * unitCount.first.whatBuilds().first.mineralPrice() + numPreBuilt * unitCount.first.mineralPrice() - minerals;
			int totalGas = (i - numMakersOwned) * unitCount.first.whatBuilds().first.gasPrice() + numPreBuilt * unitCount.first.gasPrice() - gas;
			int resourceTime = getResourceTime(totalMinerals, totalGas);
			newTime += std::max(0, resourceTime);
			minBuildTime = std::min(minBuildTime, newTime);
		}
		if (minBuildTime < 1000000000) {
			//ofs << "bottleneck: " << minBuildTime << " ";
			maxBuildTime = std::max(maxBuildTime, minBuildTime);
		}
	}
	
	// sum up resources and build times for all unbuilt units required for goal units
	for (auto &unitCount : requiredUnits)
	{
		if (unitCount.first != UnitTypes::Protoss_Pylon) {
			totalMineralsRequired += unitCount.first.mineralPrice() * unitCount.second;
			totalGasRequired += unitCount.first.gasPrice() * unitCount.second;
		}
	}

	int minTime = getResourceTime(totalMineralsRequired, totalGasRequired);
	//ofs << "resources: " << minTime << " " << totalMineralsRequired - minerals << " " << mineralWorkers;
	result = std::max(minTime, maxBuildTime);
	/*
	std::ofstream ofs;
	ofs.open("C:\\Users\\suhsa\\Source\\Repos\\starcraft\\BWAPI\\Release\\debug.txt", std::ofstream::out | std::ofstream::app);
	ofs << "        ";
	ofs << totalMineralsRequired;
	ofs << " " << minerals;
	ofs << " " << mineralWorkers;
	ofs << "\n";
	ofs.close();
	*/
	//ofs << "result: " << result << "\n";
	//ofs.close();
	return result;
}

BuildOrderAction::BuildOrderAction(UnitType unit, UpgradeType upgrade, TechType tech) : unit(unit), upgrade(upgrade), tech(tech)
{
	if (unit != UnitTypes::None)
	{
		builder = unit.whatBuilds().first;
		mineralCost = unit.mineralPrice();
		gasCost = unit.gasPrice();
		supplyCost = unit.supplyRequired();
		supplyGain = unit.supplyProvided();
		timeCost = unit.buildTime();
		// if it's a building, take into account time for a probe to make the warp rift
		if (unit.isBuilding())
		{
			timeCost += 150;
		}

		requiredUnits = unit.requiredUnits();
		// most buildings need pylons
		if (unit.isBuilding() && !(unit == UnitTypes::Protoss_Pylon || unit == UnitTypes::Protoss_Nexus || unit == UnitTypes::Protoss_Assimilator))
		{
			requiredUnits[UnitTypes::Protoss_Pylon] = 1;
		}

		builderOnMinerals = false;
		builderOnGas = false;
	}
	else if (upgrade != UpgradeTypes::None)
	{
		builder = upgrade.whatUpgrades();
		mineralCost = upgrade.mineralPrice();
		gasCost = upgrade.gasPrice();
		supplyCost = 0;
		supplyGain = 0;
		timeCost = upgrade.upgradeTime();

		requiredUnits[upgrade.whatsRequired()] = 1;

		builderOnMinerals = false;
		builderOnGas = false;
	}
	else
	{
		builder = tech.whatResearches();
		mineralCost = tech.mineralPrice();
		gasCost = tech.gasPrice();
		supplyCost = 0;
		supplyGain = 0;
		timeCost = tech.researchTime();

		requiredUnits[tech.requiredUnit()] = 1;

		builderOnMinerals = false;
		builderOnGas = false;
	}
}

/* Finds the build order with the fastest time to reach goal */
void BuildOrderManager::findBuildOrder(std::map<BWAPI::UnitType, int> goalUnits, std::map<BWAPI::UpgradeType, int> goalUpgrades, std::set<BWAPI::TechType> goalTechs)
{
	if (!goalUpgrades.empty())
	{
		for (auto &upgradeCount : goalUpgrades)
		{
			Broodwar << upgradeCount.first << std::endl;
		}
	}
	// priority queue for fringe
	std::priority_queue<BuildOrderNode, std::vector<BuildOrderNode>, NodeComparison> fringe;

	// keep track of states visited
	std::unordered_map<MacroGameState, int, MacroGameStateHasher> visited;

	// get start state
	MacroGameState startState(goalUnits, goalUpgrades, goalTechs);
	// put start state on fringe
	std::deque<BuildOrderAction> startActions;
	BuildOrderNode startNode(0, startState.buildTimeHeuristic(), startActions, startState);
	fringe.push(startNode);
	int count = 0;
	// keep searching until we run out of nodes
	while (!fringe.empty())
	{
		// if search goes on too long, stop it
		++count;
		BuildOrderNode node = fringe.top();
		fringe.pop();
		/*
		std::ofstream ofs;
		ofs.open("C:\\Users\\suhsa\\Source\\Repos\\starcraft\\BWAPI\\Release\\debug.txt", std::ofstream::out | std::ofstream::app);
		ofs << count;
		ofs << " " << node.timeCost;
		ofs << " " << node.heuristic;
		for (auto &action : node.actions) {
			ofs << " " << action.unit;
		}
		ofs << "\n";
		ofs.close();
		*/
		// if this state is a goal, return the actions that got us here
		// also, if fringe is too large, return actions that got us closest
		if (node.state.isGoalState() || count > 5000)
		{
			Broodwar << count << std::endl;
			buildOrder = node.actions;
			return;
		}
		// if we've already visited this state, and our previous path had a lower cost, move on
		if (visited.count(node.state) > 0) {
			if (node.timeCost >= visited[node.state]) {
				continue;
			}
		}
		/*
		bool moveOn = false;
		for (auto it = visited.begin(); it != visited.end();)
		{
			if (it->first == node.state && it->second <= node.timeCost)
			{
				moveOn = true;
				break;
			}
			else if (it->first == node.state) {
				it = visited.erase(it);
			}
			else {
				++it;
			}
		}
		if (moveOn) {
			continue;
		}
		*/
		// otherwise, add the state to visited states
		visited[node.state] = node.timeCost;
		// expand the state and move on
		std::vector<BuildOrderAction> nextActions = node.state.getFeasibleActions();
		for (auto &action : nextActions)
		{
			std::deque<BuildOrderAction> previousActions = node.actions;
			
			/*
			// macro: workers are made at least 2 at a time
			if (previousActions.size() > 1 && previousActions.back().unit == UnitTypes::Protoss_Probe && action.unit != UnitTypes::Protoss_Probe && previousActions.end()[-2].unit != UnitTypes::Protoss_Probe) {
				continue;
			}

			// macro: fighters are made at least 2 at a time
			if (previousActions.size() > 1 && previousActions.back().unit == UnitTypes::Protoss_Zealot && action.unit != UnitTypes::Protoss_Zealot && previousActions.end()[-2].unit != UnitTypes::Protoss_Zealot) {
				continue;
			}
			
			// macro: workers are made at beginning of order
			if (action.unit == UnitTypes::Protoss_Probe && previousActions.size() > 8) {
				continue;
			}
			*/
			
			previousActions.push_back(action);
			// if a list of actions involves building a ridiculous number of some unit,
			// there's no way it's optimal
			int pylonCount = 0;
			int coreCount = 0;
			int citadelCount = 0;
			int templarArchivesCount = 0;
			int gatewayCount = 0;
			int gatewayUnitCount = 0;
			for (auto &action2 : previousActions)
			{
				if (action2.unit == UnitTypes::Protoss_Pylon)
				{
					++pylonCount;
				}
				else if (action2.unit == UnitTypes::Protoss_Cybernetics_Core)
				{
					++coreCount;
				}
				else if (action2.unit == UnitTypes::Protoss_Citadel_of_Adun)
				{
					++citadelCount;
				}
				else if (action2.unit == UnitTypes::Protoss_Templar_Archives)
				{
					++templarArchivesCount;
				}
				else if (action2.unit == UnitTypes::Protoss_Gateway) {
					++gatewayCount;
				}
				else if (action2.unit.whatBuilds().first == UnitTypes::Protoss_Gateway) {
					++gatewayUnitCount;
				}
			}
			if (pylonCount > 1 && startState.supplyTotal + (pylonCount - 2) * UnitTypes::Protoss_Pylon.supplyProvided() > startState.supplyUsed) {
				continue;
			}
			if (coreCount > 1 || citadelCount > 1 || templarArchivesCount > 1)
			{
				continue;
			}
			/*
			if (gatewayCount > goalUnits[UnitTypes::Protoss_Zealot] + goalUnits[UnitTypes::Protoss_Dragoon] + goalUnits[UnitTypes::Protoss_Dark_Templar]) {
				continue;
			}
			if (gatewayUnitCount > 0 && action.unit == UnitTypes::Protoss_Gateway) {
				continue;
			}
			*/

			MacroGameState nextState(node.state, action);
			BuildOrderNode nextNode(node.timeCost + nextState.gameTime - node.state.gameTime, nextState.buildTimeHeuristic(), previousActions, nextState);
			fringe.push(nextNode);
			/*
			std::ofstream ofs;
			ofs.open("C:\\Users\\suhsa\\Source\\Repos\\starcraft\\BWAPI\\Release\\debug.txt", std::ofstream::out | std::ofstream::app);
			ofs << "    ";
			ofs << nextNode.timeCost;
			ofs << " " << nextNode.heuristic;
			for (auto &action : nextNode.actions) {
				ofs << " " << action.unit;
			}
			ofs << "\n";
			ofs.close();
			*/
		}
	}
}

void BuildOrderManager::executeBuildOrder()
{
	// if we've completed the current build order, make a new one
	if (buildOrder.empty())
	{
		std::map<UnitType, int> goalUnits = StrategyManager::getInstance().getUnitGoals();
		if (goalUnits.count(UnitTypes::Protoss_Citadel_of_Adun))
		{
			Broodwar << "Need citadel" << std::endl;
		}
		std::map<UpgradeType, int> goalUpgrades = StrategyManager::getInstance().getUpgradeGoals();
		std::set<TechType> goalTechs;
		findBuildOrder(goalUnits, goalUpgrades, goalTechs);
	}
	
	for (auto &action : buildOrder)
	{
		if (action.unit != UnitTypes::None)
		{
			Broodwar << action.unit;
		}
		if (action.upgrade != UpgradeTypes::None)
		{
			Broodwar << action.upgrade;
		}
	}
	Broodwar << std::endl;
	
	// iterate over each action in the build order
	while (!buildOrder.empty())
	{
		BuildOrderAction action = buildOrder.front();
		if (action.unit != UnitTypes::None)
		{
			if (buildUnit(action))
			{
				buildOrder.pop_front();
			}
			// if we can't build this part of the build order yet, stop
			else
			{
				break;
			}
		}
		else if (action.upgrade != UpgradeTypes::None)
		{
			if (buildUpgrade(action))
			{
				buildOrder.pop_front();
			}
			else
			{
				break;
			}
		}
	}
}

bool BuildOrderManager::buildUnit(BuildOrderAction action)
{
	// if we can't build this, return false
	if (!Broodwar->canMake(action.unit) || (action.unit.isBuilding() && action.unit != UnitTypes::Protoss_Pylon && action.unit != UnitTypes::Protoss_Nexus && action.unit != UnitTypes::Protoss_Assimilator && Broodwar->self()->completedUnitCount(UnitTypes::Protoss_Pylon) == 0))
	{
		//Broodwar << "Can't build:" << std::endl;
		//Broodwar << action.unit << std::endl;
		return false;
	}
	// find a unit to execute this action
	for (auto &u : Broodwar->self()->getUnits())
	{
		if (u->getType() == action.builder && (u->isIdle() || u->isGatheringGas() || u->isGatheringMinerals()))
		{
			//Broodwar << "Can build:" << std::endl;
			//Broodwar << action.unit << std::endl;
			if (action.unit == UnitTypes::Protoss_Nexus)
			{
				ExpansionManager::getInstance().giveExpansionOrders(u);
				return true;
			}
			else if (action.unit.isBuilding())
			{
				// if we're making a building, find a place to put it
				TilePosition targetBuildLocation = Broodwar->getBuildLocation(action.unit, u->getTilePosition());
				if (targetBuildLocation)
				{
					UnitType u1 = action.unit;
					// Register an event that draws the target build location
					Broodwar->registerEvent([targetBuildLocation, u1](Game*)
					{
						Broodwar->drawBoxMap(Position(targetBuildLocation),
							Position(targetBuildLocation + u1.tileSize()),
							Colors::Blue);
					},
						nullptr,  // condition
						u1.buildTime() + 100);  // frames to run

					// Order the builder to construct the supply structure
					// lock resources for the build
					if (ResourceManager::getInstance().lockResources(action.unit) && ResourceManager::getInstance().lockLand(targetBuildLocation, targetBuildLocation + u1.tileSize()))
					{
						Broodwar << "Can build: " << action.unit << std::endl;
						if (u->build(action.unit, targetBuildLocation))
						{
							return true;
						}
						// if build failed, release resources
						else
						{
							Broodwar << Broodwar->getLastError() << std::endl;
							ResourceManager::getInstance().releaseLockedResources(action.unit);
							ResourceManager::getInstance().releaseLockedLand(targetBuildLocation);
						}
					}
				}
			}
			else
			{
				// lock resources for the build
				if (ResourceManager::getInstance().lockResources(action.unit))
				{
					if (u->train(action.unit))
					{
						return true;
					}
					// if build failed, release resources
					else
					{
						Broodwar << Broodwar->getLastError() << std::endl;
						ResourceManager::getInstance().releaseLockedResources(action.unit);
					}
				}
			}
			break;
		}
	}
	return false;
}

bool BuildOrderManager::buildUpgrade(BuildOrderAction action)
{
	// if we can't build this, return false
	if (!Broodwar->canUpgrade(action.upgrade))
	{
		Broodwar << "Can't build:" << std::endl;
		Broodwar << action.upgrade << std::endl;
		return false;
	}
	// find a unit to execute this action
	for (auto &u : Broodwar->self()->getUnits())
	{
		if (u->getType() == action.builder && (u->isIdle() || u->isGatheringGas() || u->isGatheringMinerals()))
		{
			if (ResourceManager::getInstance().lockResources(action.upgrade))
			{
				Broodwar << "Can build:" << std::endl;
				Broodwar << action.upgrade << std::endl;

				if (u->upgrade(action.upgrade))
				{
					return true;
				}
				// if build failed, release resources
				else
				{
					Broodwar << Broodwar->getLastError() << std::endl;
					ResourceManager::getInstance().releaseLockedResources(action.unit);
				}
			}
			break;
		}
	}
	return false;
}