#include <BWAPI.h>
#include <mutex>

#ifndef RESOURCEMANAGER_H
#define RESOURCEMANAGER_H

class ResourceManager
{
	ResourceManager() { lockedGas = lockedMinerals = 0; }
	int lockedMinerals;
	int lockedGas;
	std::mutex lock;

public:
	static ResourceManager &getInstance()
	{
		static ResourceManager instance;
		return instance;
	}
	ResourceManager(const ResourceManager &) = delete;
	void operator=(const ResourceManager &) = delete;

	std::map<BWAPI::UnitType, std::pair<int, int>> unitsInProgressCounts; // gives how many of each unit are being built
	std::map<BWAPI::UpgradeType, int> upgradesInProgressCounts;
	std::vector<std::pair<std::pair<BWAPI::TilePosition, BWAPI::TilePosition>, int>> landLocks; // each pair is the left-top and bottom-right tiles of the region claimed
	void releaseLockedResources(BWAPI::UnitType unit);
	bool lockResources(BWAPI::UnitType unit);
	bool lockResources(BWAPI::UpgradeType upgrade);
	void releaseLockedLand(BWAPI::TilePosition leftTop);
	bool lockLand(BWAPI::TilePosition leftTop, BWAPI::TilePosition rightBottom);
	int getUnitsInProgress(BWAPI::UnitType unit);
	void checkExpirations();
};

#endif