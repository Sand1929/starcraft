#include <BWAPI.h>

#ifndef STRATEGYMANAGER_H
#define STRATEGYMANAGER_H

class StrategyManager
{
	StrategyManager() { }
	std::string strategy;
	std::vector<int> wins;
	std::vector<int> totals;
	std::map<BWAPI::UnitType, int> getTwoGateZealotUnitGoals();
	std::map<BWAPI::UnitType, int> getOneGateCoreUnitGoals();
	std::map<BWAPI::UnitType, int> getDTDropUnitGoals();
	std::map<BWAPI::UpgradeType, int> getDTDropUpgradeGoals();
	std::map<BWAPI::UnitType, int> getThreeGateRoboUnitGoals();
	std::map<BWAPI::UpgradeType, int> getThreeGateRoboUpgradeGoals();
	std::map<BWAPI::UnitType, int> getCorsairDarkTemplarUnitGoals();
	std::map<BWAPI::UpgradeType, int> getCorsairDarkTemplarUpgradeGoals();
	std::map<BWAPI::UnitType, int> getSpeedZealUnitGoals();
	std::map<BWAPI::UpgradeType, int> getSpeedZealUpgradeGoals();
	std::map<BWAPI::UnitType, int> getAirForceGoals();

	std::vector<std::vector<double>> winProbData;

public:
	static StrategyManager &getInstance()
	{
		static StrategyManager instance;
		return instance;
	}
	StrategyManager(const StrategyManager &) = delete;
	void operator=(const StrategyManager &) = delete;

	std::map<BWAPI::UnitType, int> getUnitGoals();
	std::map<BWAPI::UpgradeType, int> getUpgradeGoals();
	std::string getStrategy();
	void decideStrategy();
	void updateStrategy(bool gameWon);
};

#endif