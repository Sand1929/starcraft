#include <BWAPI.h>

#ifndef EXPANSIONMANAGER_H
#define EXPANSIONMANAGER_H

class ExpansionManager
{
	ExpansionManager();
	int supplyTrigger; // amount of supply at which to send out a scout
	int numExpansions; // number of expansions

public:
	static ExpansionManager &getInstance()
	{
		static ExpansionManager instance;
		return instance;
	}
	ExpansionManager(const ExpansionManager &) = delete;
	void operator=(const ExpansionManager &) = delete;

	std::set<BWAPI::Position> mineralLocations;
	std::set<BWAPI::Position> gasLocations;
	std::set<BWAPI::Position> enemyBuildingLocations;
	std::map<BWAPI::UnitType, int> enemyCount;

	BWAPI::TilePosition pickExpansion();
	bool giveExpansionOrders(BWAPI::Unit unit);
	int getNumExpansions();
};

#endif