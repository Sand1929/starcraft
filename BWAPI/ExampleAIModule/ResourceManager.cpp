#include "ResourceManager.h"

using namespace BWAPI;

/* Release locked resources, but don't let locked resources fall below 0 */
void ResourceManager::releaseLockedResources(UnitType unit)
{
	lockedMinerals = std::max(lockedMinerals - unit.mineralPrice(), 0);
	lockedGas = std::max(lockedGas - unit.gasPrice(), 0);
	unitsInProgressCounts[unit].first -= 1;
	if (unitsInProgressCounts[unit].first <= 0)
	{
		unitsInProgressCounts.erase(unit);
	}
}

/* Try to lock resources. If successful, return true */
bool ResourceManager::lockResources(UnitType unit)
{
	// there's a bug in the API where assimilators don't activate onCreate
	if (unitsInProgressCounts[UnitTypes::Protoss_Assimilator].first >= 1 && Broodwar->self()->allUnitCount(UnitTypes::Protoss_Assimilator) >= 1)
	{
		releaseLockedResources(UnitTypes::Protoss_Assimilator);
	}
	lock.lock();
	if (unit.mineralPrice() <= Broodwar->self()->minerals() - lockedMinerals && unit.gasPrice() <= Broodwar->self()->gas() - lockedGas)
	{
		lockedMinerals += unit.mineralPrice();
		lockedGas += unit.gasPrice();
		unitsInProgressCounts[unit].first += 1;
		lock.unlock();
		return true;
	}
	lock.unlock();
	return false;
}

/* Try to lock resources. If successful, return true */
bool ResourceManager::lockResources(UpgradeType upgrade)
{
	lock.lock();
	if (upgrade.mineralPrice() <= Broodwar->self()->minerals() - lockedMinerals && upgrade.gasPrice() <= Broodwar->self()->gas() - lockedGas)
	{
		lockedMinerals += upgrade.mineralPrice();
		lockedGas += upgrade.gasPrice();
		lock.unlock();
		return true;
	}
	lock.unlock();
	return false;
}

void ResourceManager::releaseLockedLand(TilePosition leftTop)
{
	lock.lock();
	// iterate over locks and remove one that matches this one, if any
	std::vector<std::pair<std::pair<TilePosition, TilePosition>, int>>::iterator iter = landLocks.begin();
	while (iter != landLocks.end())
	{
		if (iter->first.first == leftTop)
		{
			landLocks.erase(iter);
			break;
		}
		++iter;
	}
	lock.unlock();
}

bool ResourceManager::lockLand(TilePosition leftTop, TilePosition bottomRight)
{
	lock.lock();
	// check if this land is already locked
	for (auto &landLock : landLocks)
	{
		if (leftTop.x < landLock.first.second.x && bottomRight.x > landLock.first.first.x && leftTop.y < landLock.first.second.y && bottomRight.y > landLock.first.first.y)
		{
			lock.unlock();
			return false;
		}
	}
	// if we don't conflict with any existing locks, add a lock
	landLocks.push_back(std::make_pair(std::make_pair(leftTop, bottomRight), 0));
	lock.unlock();
	return true;
}

int ResourceManager::getUnitsInProgress(UnitType unit)
{
	return unitsInProgressCounts[unit].first;
}

void ResourceManager::checkExpirations()
{
	// check for expired resource locks
	for (auto it = unitsInProgressCounts.begin(); it != unitsInProgressCounts.end();)
	{
		it->second.second += 1;

		if ((it->second.second > 500 && it->first != UnitTypes::Protoss_Nexus) || it->second.second > 2500)
		{
			lockedMinerals = std::max(lockedMinerals - it->first.mineralPrice(), 0);
			lockedGas = std::max(lockedGas - it->first.gasPrice(), 0);
			it->second.first -= 1;
			if (it->second.first <= 0)
			{
				it = unitsInProgressCounts.erase(it);
				continue;
			}
			it->second.second = 0;
		}
		++it;
	}
	// check for expired land locks
	lock.lock();
	std::vector<std::pair<std::pair<BWAPI::TilePosition, BWAPI::TilePosition>, int>>::iterator iter;
	for (iter = landLocks.begin(); iter != landLocks.end();)
	{
		iter->second += 1;
		if (iter->second > 500)
		{
			iter = landLocks.erase(iter);
		}
		else
		{
			++iter;
		}
	}
	lock.unlock();
	// check for expired upgrade resource locks
	for (auto it = upgradesInProgressCounts.begin(); it != upgradesInProgressCounts.end();)
	{
		it->second += 1;

		if (it->second > 100)
		{
			lockedMinerals = std::max(lockedMinerals - it->first.mineralPrice(), 0);
			lockedGas = std::max(lockedGas - it->first.gasPrice(), 0);
			it = upgradesInProgressCounts.erase(it);
		}
		else
		{
			++it;
		}
	}
}