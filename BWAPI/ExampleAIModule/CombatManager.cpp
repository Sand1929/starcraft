#include "CombatManager.h"
#include "StrategyManager.h"
#include "ScoutManager.h"
#include <time.h>  

using namespace BWAPI;

CombatManager::CombatManager()
{
	/*
	if (StrategyManager::getInstance().getStrategy() == "ThreeGateRobo")
	{
		squadSize = 3;
	}
	else if (StrategyManager::getInstance().getStrategy() == "DTDrop")
	{
		squadSize = 2;
	}
	else if (StrategyManager::getInstance().getStrategy() == "CorsairDarkTemplar")
	{
		squadSize = 2;
	}
	else if (StrategyManager::getInstance().getStrategy() == "SpeedZeal")
	{
		squadSize = 5;
	}
	else
	{
		squadSize = 0;
	}
	*/
	increaseSquadSize = false; 
	cleanUpMode = false;
}

/* Gives orders to the given worker unit */
void CombatManager::giveWorkerOrders(Unit u, bool collectGas)
{
	// if the given unit isn't a worker, stop
	if (!u->getType().isWorker())
	{
		return;
	}

	if (u->getID() == ScoutManager::getInstance().getScoutID())
	{
		ScoutManager::getInstance().giveScoutOrders();
	}

	// if there's an enemy probe doing a cannon rush or gas steal or something in the base, attack it
	Unit closestEnemy = u->getClosestUnit(Filter::IsEnemy, 500);
	if (closestEnemy && (closestEnemy->getType().isWorker() || closestEnemy->getType().isBuilding() || u->getDistance(closestEnemy) < 50) && !u->isAttacking() && u->canAttackUnit(closestEnemy))
	{
		if (!u->attack(closestEnemy))
		{
			Broodwar << Broodwar->getLastError() << std::endl;
		}
		return;
	}
	// if our worker is idle
	if (u->isIdle())
	{
		// Order workers carrying a resource to return them to the center,
		// otherwise find a mineral patch to harvest.
		if (u->isCarryingGas() || u->isCarryingMinerals())
		{
			u->returnCargo();
			return;
		}
		if (!u->getPowerUp())  // The worker cannot harvest anything if it
		{                             // is carrying a powerup such as a flag
			// if collecting gas, harvest gas only if you can and we maintain a 4:1 ratio of minerals to gas
			// and we don't already have 3 on gas
			if (collectGas && Broodwar->self()->completedUnitCount(UnitTypes::Protoss_Assimilator) > 0)
			{
				int gasWorkers = 0;
				int mineralWorkers = 0;
				for (auto &unit : Broodwar->self()->getUnits())
				{
					if (unit->isGatheringGas() || unit->isCarryingGas())
					{
						++gasWorkers;
					}
					if (unit->isGatheringMinerals() || unit->isCarryingMinerals())
					{
						++mineralWorkers;
					}
				}
				if (mineralWorkers > 8 && gasWorkers < 3)
				{
					if (!u->gather(u->getClosestUnit(Filter::IsRefinery)))
					{
						// If the call fails, then print the last error message
						Broodwar << Broodwar->getLastError() << std::endl;
					}
					return;
				}
			}
			// Harvest from the nearest mineral patch
			if (!u->gather(u->getClosestUnit(Filter::IsMineralField)))
			{
				// If the call fails, then print the last error message
				Broodwar << Broodwar->getLastError() << std::endl;
			}
		} // closure: has no powerup
	} // closure: if idle
}

bool CombatManager::shouldRetreat(Unit unit)
{
	// if we're fighting at our base, we're too close to retreat
	if (unit->getDistance(Position(Broodwar->self()->getStartLocation())) < 300)
	{
		return false;
	}
	// calculate total hit points and attack power of this unit and nearby allied fighters
	int alliedHitPoints = unit->getHitPoints() + unit->getShields();
	float alliedAttackPower = (float)unit->getType().groundWeapon().damageAmount() / unit->getType().groundWeapon().damageCooldown();
	for (auto &ally : unit->getUnitsInRadius(350, Filter::IsOwned))
	{
		if (ally->getType().canAttack() && !ally->isCloaked())
		{
			alliedHitPoints += ally->getHitPoints() + ally->getShields();
			alliedAttackPower += (float)ally->getType().groundWeapon().damageAmount() / ally->getType().groundWeapon().damageCooldown();
		}
	}
	// if close to a position where we encountered strong resistance, don't go there
	// if record of enemy position is old, delete it
	enemyPositions.erase(std::remove_if(enemyPositions.begin(), enemyPositions.end(), [](EnemyPosition pos) { return Broodwar->getFrameCount() - pos.timeRecorded > 200; }), enemyPositions.end());
	for (auto &position : enemyPositions) {
		if (unit->getDistance(position.position) < 800 && alliedHitPoints * alliedAttackPower <= position.strength) {
			return true;
		}
	}

	// calculate hit points and attack power of nearby enemies
	int enemyHitPoints = 0;
	float enemyAttackPower = 0;
	bool mobileEnemiesNearby = false;
	for (auto &enemy : unit->getUnitsInRadius(500, Filter::IsEnemy))
	{
		if (enemy->getType().canAttack() && enemy->isCompleted() && !enemy->getType().isWorker() && !enemy->getType().isBuilding())
		{
			if (enemy->getDistance(unit) < 200)
			{
				mobileEnemiesNearby = true;
			}
			enemyHitPoints += enemy->getHitPoints() + enemy->getShields();
			enemyAttackPower += (float)enemy->getType().groundWeapon().damageAmount() / enemy->getType().groundWeapon().damageCooldown();
		}
	}

	// add in enemy fortifications
	for (auto &enemyRecord : CombatManager::getInstance().enemyFortifications)
	{
		if (unit->getDistance(enemyRecord.first) < enemyRecord.second.groundWeapon().maxRange() + 100)
		{
			enemyHitPoints += enemyRecord.second.maxHitPoints() + enemyRecord.second.maxShields();
			enemyAttackPower += (float)enemyRecord.second.groundWeapon().damageAmount() / enemyRecord.second.groundWeapon().damageCooldown();
		}
	}
	
	// if enemy is stronger, add to records and don't attack
	if (alliedHitPoints * alliedAttackPower <= enemyHitPoints * enemyAttackPower)
	{
		EnemyPosition position(unit->getPosition(), enemyHitPoints * enemyAttackPower, Broodwar->getFrameCount());
		enemyPositions.push_back(position);
		return true;
	}
	return false;
}

/* Give orders to fighting units */
void CombatManager::giveStandardAttackOrders(Unit u, bool invisibleUnits, int altSquadSize)
{
	if (cleanUpMode)
	{
		// just attack the closest enemy if there is one
		Unit closestEnemy = u->getClosestUnit(Filter::IsEnemy);
		if (closestEnemy && u->canAttackUnit(closestEnemy))
		{
			u->attack(closestEnemy);
		}
		// otherwise, wander around to find enemies
		else if ((u->getVelocityX() == 0 && u->getVelocityY() == 0) || u->isIdle())
		{
			// try areas where enemy buildings were sighted before
			for (auto &enemyPosition : ScoutManager::getInstance().enemyBuildingLocations)
			{
				u->attack(enemyPosition);
				return;
			}

			srand(time(NULL));
			int i = rand() % Broodwar->mapWidth();
			int j = rand() % Broodwar->mapHeight();
			if (!Broodwar->isVisible(i, j) && u->hasPath(Position(i * 32, j * 32)))
			{
				u->attack(Position(i * 32, j * 32));
			}
		}
	}
	else
	{
		// if near base, and base is under attack, defend
		if (u->getDistance(Position(Broodwar->self()->getStartLocation())) < 500) {
			Unit closestEnemy = u->getClosestUnit(Filter::IsEnemy, 500);
			if (closestEnemy && closestEnemy->getDistance(Position(Broodwar->self()->getStartLocation())) < 500) {
				if (!u->attack(closestEnemy))
				{
					Broodwar << Broodwar->getLastError() << std::endl;
				}
				return;
			}
		}
		Unitset enemies = u->getUnitsInRadius(500, Filter::IsEnemy);
		// if we haven't found enemies, group up and move toward the opponent's starting location
		if (enemies.empty())
		{
			// count number of grouped up Zealots, Dragoons, and Dark Templars
			int fighterCount = 0;
			for (auto &unit : u->getUnitsInRadius(200, Filter::IsOwned))
			{
				UnitType ut = unit->getType();
				if (ut == UnitTypes::Protoss_Zealot || ut == UnitTypes::Protoss_Dragoon || ut == UnitTypes::Protoss_Dark_Templar)
				{
					++fighterCount;
				}
			}
			if ((altSquadSize >= 0 && fighterCount >= altSquadSize) || (altSquadSize == -1 && fighterCount >= CombatManager::getInstance().squadSize))
			{
				// iterate over starting positions
				for (auto &location : Broodwar->getStartLocations())
				{
					// if location is not our start location, it's the enemy's, so go there
					if (location != Broodwar->self()->getStartLocation())
					{
						// if we are not at the enemy's start location, attack it
						if (u->getDistance(Position(location)) > 100)
						{
							if (!u->attack(Position(location)))
							{
								Broodwar << Broodwar->getLastError() << std::endl;
							}
						}
						else
						{
							// else just attack the some unit
							Unit closestEnemy = u->getClosestUnit(Filter::IsEnemy);
							if (closestEnemy)
							{
								if (!u->attack(closestEnemy))
								{
									Broodwar << Broodwar->getLastError() << std::endl;
								}
							}
						}
					}
				}
			}
		}
		// else attack!
		else
		{
			for (auto &enemy : enemies)
			{
				// prioritize medics
				if (enemy->getType() == UnitTypes::Terran_Medic)
				{
					if (u->attack(enemy))
					{
						return;
					}
				}
			}
			// if this strategy involves invisible units, target detectors next
			if (invisibleUnits)
			{
				for (auto &enemy : enemies)
				{
					if (enemy->getType().isDetector() || enemy->getType() == UnitTypes::Terran_Comsat_Station)
					{
						if (u->attack(enemy))
						{
							return;
						}
					}
				}
			}
			for (auto &enemy : enemies)
			{
				UnitType ut = enemy->getType();
				// prioritize buildings that can hurt you next, unless they are being repaired
				if ((ut == UnitTypes::Zerg_Sunken_Colony || ut == UnitTypes::Zerg_Spore_Colony || ut == UnitTypes::Zerg_Creep_Colony || ut == UnitTypes::Terran_Bunker || ut == UnitTypes::Terran_Missile_Turret || ut == UnitTypes::Protoss_Photon_Cannon) && !enemy->isBeingHealed())
				{
					if (u->attack(enemy))
					{
						return;
					}
				}
			}
			for (auto &enemy : enemies)
			{
				// prioritize other units that can hurt you next
				if (enemy->canAttack() && !enemy->getType().isWorker())
				{
					if (u->attack(enemy))
					{
						return;
					}
				}
			}
			for (auto &enemy : enemies)
			{
				// prioritize workers next
				if (enemy->getType().isWorker())
				{
					if (u->attack(enemy))
					{
						return;
					}
				}
			}
			// if there's nothing that can hurt you, just attack the closest enemy
			Unit closestEnemy = u->getClosestUnit(Filter::IsEnemy);
			if (closestEnemy && u->canAttackUnit(closestEnemy))
			{
				if (u->attack(closestEnemy))
				{
					return;
				}
			}
		}
		
	}
}

/* Check if we should be in cleanup mode */
void CombatManager::standardCleanUpModeCheck()
{
	// if not already in cleanup mode, check if we should be in cleanup mode
	if (!cleanUpMode)
	{
		cleanUpMode = true;
		bool atEnemyStart = false;
		for (auto &unit : Broodwar->self()->getUnits())
		{
			// if units at the enemy's starting location can find no enemies, clean up
			for (auto &location : Broodwar->getStartLocations())
			{
				// if location is not our start location, it's the enemy's
				if (location != Broodwar->self()->getStartLocation())
				{
					if (unit->getDistance(Position(location)) < 100)
					{
						atEnemyStart = true;
						Unitset closeEnemies = unit->getUnitsInRadius(300, Filter::IsEnemy);
						for (auto &enemy : closeEnemies)
						{
							if (enemy->getType().isBuilding() || enemy->getType().canAttack())
							{
								cleanUpMode = false;
								break;
							}
						}
					}
				}
			}
			if (!cleanUpMode)
			{
				break;
			}
		}
		// don't clean up if we haven't reached the enemy base
		if (!atEnemyStart)
		{
			cleanUpMode = false;
		}
	}
}

void CombatManager::giveTwoGateZealotRushOrders()
{
	standardCleanUpModeCheck();

	// Iterate through all the units that we own
	for (auto &u : Broodwar->self()->getUnits())
	{
		// Ignore the unit if it no longer exists
		// Make sure to include this block when handling any Unit pointer!
		if (!u->exists())
			continue;

		// Ignore the unit if it has one of the following status ailments
		if (u->isLockedDown() || u->isMaelstrommed() || u->isStasised())
			continue;

		// Ignore the unit if it is in one of the following states
		if (u->isLoaded() || !u->isPowered() || u->isStuck())
			continue;

		// Ignore the unit if it is incomplete or busy constructing
		if (!u->isCompleted() || u->isConstructing())
			continue;

		// Finally make the unit do some stuff!

		// If the unit is a worker unit
		if (u->getType().isWorker())
		{
			giveWorkerOrders(u, false);
		}
		else if (u->getType() == UnitTypes::Protoss_Zealot)
		{
			// if we should retreat, run back to base!
			if (shouldRetreat(u))
			{
				u->move(Position(Broodwar->self()->getStartLocation()));
			}
			else if (u->isIdle() || TilePosition(u->getOrderTargetPosition()) == Broodwar->self()->getStartLocation())
			{
				giveStandardAttackOrders(u);
			}
		}
	} 
}

void CombatManager::giveSpeedZealOrders()
{
	standardCleanUpModeCheck();

	// Iterate through all the units that we own
	for (auto &u : Broodwar->self()->getUnits())
	{
		// Ignore the unit if it no longer exists
		// Make sure to include this block when handling any Unit pointer!
		if (!u->exists())
			continue;

		// Ignore the unit if it has one of the following status ailments
		if (u->isLockedDown() || u->isMaelstrommed() || u->isStasised())
			continue;

		// Ignore the unit if it is in one of the following states
		if (u->isLoaded() || !u->isPowered() || u->isStuck())
			continue;

		// Ignore the unit if it is incomplete or busy constructing
		if (!u->isCompleted() || u->isConstructing())
			continue;

		// Finally make the unit do some stuff!

		// If the unit is a worker unit
		if (u->getType().isWorker())
		{
			giveWorkerOrders(u);
		}
		else if ((u->getType() == UnitTypes::Protoss_Zealot || u->getType() == UnitTypes::Protoss_Dark_Templar))
		{
			// if we should retreat, run back to base!
			if (shouldRetreat(u))
			{
				u->move(Position(Broodwar->self()->getStartLocation()));
			}
			else if (u->isIdle() || TilePosition(u->getOrderTargetPosition()) == Broodwar->self()->getStartLocation())
			{
				giveStandardAttackOrders(u);
			}
		}
	}
}

void CombatManager::giveOneGateCoreOrders()
{
	standardCleanUpModeCheck();

	// Iterate through all the units that we own
	for (auto &u : Broodwar->self()->getUnits())
	{
		// Ignore the unit if it no longer exists
		// Make sure to include this block when handling any Unit pointer!
		if (!u->exists())
			continue;

		// Ignore the unit if it has one of the following status ailments
		if (u->isLockedDown() || u->isMaelstrommed() || u->isStasised())
			continue;

		// Ignore the unit if it is in one of the following states
		if (u->isLoaded() || !u->isPowered() || u->isStuck())
			continue;

		// Ignore the unit if it is incomplete or busy constructing
		if (!u->isCompleted() || u->isConstructing())
			continue;


		// Finally make the unit do some stuff!

		// If the unit is a worker unit
		if (u->getType().isWorker())
		{
			giveWorkerOrders(u);
		}
		else if (u->getType() == UnitTypes::Protoss_Zealot)
		{
			// if we should retreat, run back to base!
			if (shouldRetreat(u))
			{
				u->move(Position(Broodwar->self()->getStartLocation()));
			}
			else if (u->isIdle() || TilePosition(u->getOrderTargetPosition()) == Broodwar->self()->getStartLocation())
			{
				giveStandardAttackOrders(u);
			}
		}

		else if (u->getType() == UnitTypes::Protoss_Dragoon && (u->isIdle() || u->isFollowing()))
		{
			// if there's an enemy nearby, attack
			Unitset enemies = u->getUnitsInWeaponRange(u->getType().groundWeapon(), Filter::IsEnemy);
			if (!enemies.empty())
			{
				for (auto & enemy : enemies)
				{
					u->attack(enemy);
					break;
				}
			}
			// otherwise, follow a nearby zealot
			else
			{
				for (auto &unit : u->getUnitsInRadius(1000, Filter::IsOwned))
				{
					if (unit->getType() == UnitTypes::Protoss_Zealot)
					{
						u->follow(unit);
						break;
					}
				}
			}
		}
	}
}

void CombatManager::giveDTDropOrders()
{
	standardCleanUpModeCheck();

	// Iterate through all the units that we own
	for (auto &u : Broodwar->self()->getUnits())
	{
		// Ignore the unit if it no longer exists
		// Make sure to include this block when handling any Unit pointer!
		if (!u->exists())
			continue;

		// Ignore the unit if it has one of the following status ailments
		if (u->isLockedDown() || u->isMaelstrommed() || u->isStasised())
			continue;

		// Ignore the unit if it is in one of the following states
		if (u->isLoaded() || !u->isPowered() || u->isStuck())
			continue;

		// Ignore the unit if it is incomplete or busy constructing
		if (!u->isCompleted() || u->isConstructing())
			continue;


		// Finally make the unit do some stuff!

		// If the unit is a worker unit
		if (u->getType().isWorker())
		{
			giveWorkerOrders(u);
		}
		else if ((u->getType() == UnitTypes::Protoss_Dark_Templar) && !u->isAttackFrame())
		{
			// if detected, run away!
			bool detected = false;
			Unitset enemies = u->getUnitsInRadius(400, Filter::IsEnemy);
			for (auto &enemy : enemies)
			{
				if (enemy->getType().isDetector() && enemy->isCompleted())
				{
					u->move(Position(Broodwar->self()->getStartLocation()));
					detected = true;
					break;
				}
			}
			if (detected)
			{
				continue;
			}
			//giveStandardAttackOrders(u, true);
			if (cleanUpMode)
			{
				// just attack the closest enemy if there is one
				Unit closestEnemy = u->getClosestUnit(Filter::IsEnemy);
				if (closestEnemy && u->canAttackUnit(closestEnemy))
				{
					u->attack(closestEnemy);
				}
				// otherwise, wander around to find enemies
				else if (u->getVelocityX() == 0 && u->getVelocityY() == 0)
				{
					srand(time(NULL));
					int i = rand() % Broodwar->mapWidth();
					int j = rand() % Broodwar->mapHeight();
					if (!Broodwar->isVisible(i, j) && u->hasPath(Position(i * 32, j * 32)))
					{
						u->attack(Position(i * 32, j * 32));
					}
				}
			}
			else
			{
				Unitset enemies = u->getUnitsInRadius(500, Filter::IsEnemy);
				// if we haven't found enemies, move toward the opponent's starting location
				if (enemies.empty())
				{
					// iterate over starting positions
					for (auto &location : Broodwar->getStartLocations())
					{
						// if location is not our start location, it's the enemy's, so go there
						if (location != Broodwar->self()->getStartLocation())
						{
							// if we are not at the enemy's start location, attack it
							if (u->getDistance(Position(location)) > 100)
							{
								if (!u->attack(Position(location)))
								{
									Broodwar << Broodwar->getLastError() << std::endl;
								}
							}
							else
							{
								// else just attack the some unit
								Unit closestEnemy = u->getClosestUnit(Filter::IsEnemy);
								if (closestEnemy)
								{
									if (!u->attack(closestEnemy))
									{
										Broodwar << Broodwar->getLastError() << std::endl;
									}
								}
							}
						}
					}
				}
				// else attack!
				else
				{
					// if the enemy is not within range of a detector, attack
					Unit closestEnemy = u->getClosestUnit(Filter::IsEnemy);
					bool nearDetector = false;
					for (auto &detector : CombatManager::getInstance().detectors)
					{
						if (closestEnemy->getDistance(detector) <= 300)
						{
							nearDetector = true;
							break;
						}
					}
					if (!nearDetector && closestEnemy->isDetected())
					{
						u->attack(closestEnemy);
					}
				}
			}
		}
		else if (u->getType() == UnitTypes::Protoss_Zealot)
		{
			// if we should retreat, run back to base!
			if (shouldRetreat(u))
			{
				u->move(Position(Broodwar->self()->getStartLocation()));
			}
			else if (u->isIdle() || TilePosition(u->getOrderTargetPosition()) == Broodwar->self()->getStartLocation())
			{
				giveStandardAttackOrders(u);
			}
		}
		else if (u->getType() == UnitTypes::Protoss_Observer && u->isIdle())
		{
			// follow a nearby zealot
			for (auto &unit : u->getUnitsInRadius(1000, Filter::IsOwned))
			{
				if (unit->getType() == UnitTypes::Protoss_Zealot)
				{
					u->follow(unit);
					break;
				}
			}
		}
		else if (u->getType() == UnitTypes::Protoss_Shuttle && u->getLoadedUnits().size() > 1)
		{
			// drop the units at the enemy's base
			for (auto &location : Broodwar->getStartLocations())
			{
				// if location is not our start location, it's the enemy's
				if (location != Broodwar->self()->getStartLocation())
				{
					if (u->getDistance(Position(location)) < 100)
					{
						u->unloadAll();
					}
					else 
					{
						u->patrol(Position(location));
					}
				}
			}
		}
	}
}

void CombatManager::giveThreeGateRoboOrders()
{
	standardCleanUpModeCheck();

	// Iterate through all the units that we own
	for (auto &u : Broodwar->self()->getUnits())
	{
		// Ignore the unit if it no longer exists
		// Make sure to include this block when handling any Unit pointer!
		if (!u->exists())
			continue;

		// Ignore the unit if it has one of the following status ailments
		if (u->isLockedDown() || u->isMaelstrommed() || u->isStasised())
			continue;

		// Ignore the unit if it is in one of the following states
		if (u->isLoaded() || !u->isPowered() || u->isStuck())
			continue;

		// Ignore the unit if it is incomplete or busy constructing
		if (!u->isCompleted() || u->isConstructing())
			continue;


		// Finally make the unit do some stuff!

		// If the unit is a worker unit
		if (u->getType().isWorker())
		{
			giveWorkerOrders(u);
		}
		else if ((u->getType() == UnitTypes::Protoss_Reaver || u->getType() == UnitTypes::Protoss_Dragoon))
		{
			// if we should retreat, run back to base!
			if (shouldRetreat(u))
			{
				u->move(Position(Broodwar->self()->getStartLocation()));
			}
			else if (u->isIdle() || TilePosition(u->getOrderTargetPosition()) == Broodwar->self()->getStartLocation())
			{
				giveStandardAttackOrders(u);
			}
		}
		else if (u->getType() == UnitTypes::Protoss_Observer && u->isIdle())
		{
			// follow the closest dragoon or reaver
			for (auto &unit : u->getUnitsInRadius(1000, Filter::IsOwned))
			{
				if (unit->getType() == UnitTypes::Protoss_Dragoon || unit->getType() == UnitTypes::Protoss_Reaver)
				{
					u->follow(unit);
					break;
				}
			}
		}
	}
}

void CombatManager::giveCorsairDarkTemplarOrders()
{
	standardCleanUpModeCheck();

	// Iterate through all the units that we own
	for (auto &u : Broodwar->self()->getUnits())
	{
		// Ignore the unit if it no longer exists
		// Make sure to include this block when handling any Unit pointer!
		if (!u->exists())
			continue;

		// Ignore the unit if it has one of the following status ailments
		if (u->isLockedDown() || u->isMaelstrommed() || u->isStasised())
			continue;

		// Ignore the unit if it is in one of the following states
		if (u->isLoaded() || !u->isPowered() || u->isStuck())
			continue;

		// Ignore the unit if it is incomplete or busy constructing
		if (!u->isCompleted() || u->isConstructing())
			continue;


		// Finally make the unit do some stuff!

		// If the unit is a worker unit
		if (u->getType().isWorker())
		{
			giveWorkerOrders(u);
		}
		else if ((u->getType() == UnitTypes::Protoss_Dark_Templar) && !u->isAttackFrame())
		{
			// if detected, run away!
			bool detected = false;
			Unitset enemies = u->getUnitsInRadius(400, Filter::IsEnemy);
			for (auto &enemy : enemies)
			{
				if (enemy->getType().isDetector() && enemy->isCompleted())
				{
					u->move(Position(Broodwar->self()->getStartLocation()));
					detected = true;
					break;
				}
			}
			if (detected)
			{
				continue;
			}
			//giveStandardAttackOrders(u, true);
			if (cleanUpMode)
			{
				// just attack the closest enemy if there is one
				Unit closestEnemy = u->getClosestUnit(Filter::IsEnemy);
				if (closestEnemy && u->canAttackUnit(closestEnemy))
				{
					u->attack(closestEnemy);
				}
				// otherwise, wander around to find enemies
				else if (u->getVelocityX() == 0 && u->getVelocityY() == 0)
				{
					srand(time(NULL));
					int i = rand() % Broodwar->mapWidth();
					int j = rand() % Broodwar->mapHeight();
					if (!Broodwar->isVisible(i, j) && u->hasPath(Position(i * 32, j * 32)))
					{
						u->attack(Position(i * 32, j * 32));
					}
				}
			}
			else
			{
				Unitset enemies = u->getUnitsInRadius(500, Filter::IsEnemy);
				// if we haven't found enemies, move toward the opponent's starting location
				if (enemies.empty())
				{
					// iterate over starting positions
					for (auto &location : Broodwar->getStartLocations())
					{
						// if location is not our start location, it's the enemy's, so go there
						if (location != Broodwar->self()->getStartLocation())
						{
							// if we are not at the enemy's start location, attack it
							if (u->getDistance(Position(location)) > 100)
							{
								if (!u->attack(Position(location)))
								{
									Broodwar << Broodwar->getLastError() << std::endl;
								}
							}
							else
							{
								// else just attack the some unit
								Unit closestEnemy = u->getClosestUnit(Filter::IsEnemy);
								if (closestEnemy)
								{
									if (!u->attack(closestEnemy))
									{
										Broodwar << Broodwar->getLastError() << std::endl;
									}
								}
							}
						}
					}
				}
				// else attack!
				else
				{
					// if the enemy is not within range of a detector, attack
					Unit closestEnemy = u->getClosestUnit(Filter::IsEnemy);
					bool nearDetector = false;
					for (auto &detector : CombatManager::getInstance().detectors)
					{
						if (closestEnemy->getDistance(detector) <= 300)
						{
							nearDetector = true;
							break;
						}
					}
					if (!nearDetector && closestEnemy->isDetected())
					{
						u->attack(closestEnemy);
					}
				}
			}
		}
		else if (u->getType() == UnitTypes::Protoss_Zealot)
		{
			// if we should retreat, run back to base!
			if (shouldRetreat(u))
			{
				u->move(Position(Broodwar->self()->getStartLocation()));
			}
			else if (u->isIdle() || TilePosition(u->getOrderTargetPosition()) == Broodwar->self()->getStartLocation())
			{
				giveStandardAttackOrders(u);
			}
		}
		else if (u->getType() == UnitTypes::Protoss_Observer && u->isIdle())
		{
			// follow a nearby zealot
			for (auto &unit : u->getUnitsInRadius(1000, Filter::IsOwned))
			{
				if (unit->getType() == UnitTypes::Protoss_Zealot)
				{
					u->follow(unit);
					break;
				}
			}
		}
		else if (u->getType() == UnitTypes::Protoss_Corsair && !u->isAttackFrame())
		{
			// if you're near your escorts and not under attack by an unattackable enemy, 
			// see if there's a nearby unit to attack
			if (u->getUnitsInRadius(400, Filter::IsOwned).size() && !u->isUnderAttack())
			{
				for (auto &enemy : u->getUnitsInRadius(400, Filter::IsEnemy))
				{
					if (u->canAttackUnit(enemy))
					{
						u->attack(enemy);
						return;
					}
				}
			}
			// follow a nearby zealot
			for (auto &unit : Broodwar->self()->getUnits())
			{
				if (unit->getType() == UnitTypes::Protoss_Zealot)
				{
					u->follow(unit);
					return;
				}
			}
		}
	}
}

void CombatManager::giveOrders()
{
	if (StrategyManager::getInstance().getStrategy() == "TwoGateZealotRush")
	{
		giveTwoGateZealotRushOrders();
	}
	else if (StrategyManager::getInstance().getStrategy() == "CorsairDarkTemplar")
	{
		giveCorsairDarkTemplarOrders();
	}
	else if (StrategyManager::getInstance().getStrategy() == "DTDrop")
	{
		giveDTDropOrders();
	}
	else if (StrategyManager::getInstance().getStrategy() == "SpeedZeal")
	{
		giveSpeedZealOrders();
	}
	else
	{
		giveThreeGateRoboOrders();
	}
}

void CombatManager::toggleIncreaseSquadSize()
{
	/*
	// only increase the squad size every other time a unit dies
	if (increaseSquadSize)
	{
		if (StrategyManager::getInstance().getStrategy() == "TwoGateZealotRush")
		{
			squadSize = std::min(squadSize + 1, 2 * Broodwar->self()->allUnitCount(UnitTypes::Protoss_Gateway));
		}
		else if (StrategyManager::getInstance().getStrategy() == "DTDrop")
		{
			squadSize = std::min(squadSize + 1, 4 * Broodwar->self()->allUnitCount(UnitTypes::Protoss_Gateway));
		}
		else if (StrategyManager::getInstance().getStrategy() == "CorsairDarkTemplar")
		{
			squadSize = std::min(squadSize + 1, 4 * Broodwar->self()->allUnitCount(UnitTypes::Protoss_Gateway));
		}
		else
		{
			squadSize = std::min(squadSize + 1, 7);
		}
	}
	else
	{
		increaseSquadSize = true;
	}
	*/
}

int CombatManager::getSquadSize()
{
	return squadSize;
}