#include <BWAPI.h>

#ifndef SCOUTMANAGER_H
#define SCOUTMANAGER_H

class ScoutManager
{
	ScoutManager();
	int scoutID;
	BWAPI::UnitType scoutType;
	int supplyTrigger; // amount of supply at which to send out a scout

public:
	static ScoutManager &getInstance()
	{
		static ScoutManager instance;
		return instance;
	}
	ScoutManager(const ScoutManager &) = delete;
	void operator=(const ScoutManager &) = delete;

	std::set<BWAPI::Position> mineralLocations;
	std::set<BWAPI::Position> gasLocations;
	std::set<BWAPI::Position> enemyBuildingLocations;
	std::map<BWAPI::UnitType, int> enemyCount;
	std::map<BWAPI::UnitType, int> enemyHiddenCount;
	int numFlying;
	int numEnemyFlying;
	int numHiddenEnemyFlying;
	int numInvisible;
	int numEnemyInvisible;
	int numHiddenEnemyInvisible;
	int numDetectors;
	int numEnemyDetectors;
	int numHiddenEnemyDetectors;
	int numWorkers;
	int numEnemyWorkers;
	int numHiddenEnemyWorkers;
	int numFighters;
	int numEnemyFighters;
	int numHiddenEnemyFighters;
	int numRanged;
	int numEnemyRanged;
	int numHiddenEnemyRanged;
	int numBases;
	int numEnemyBases;
	int numHiddenEnemyBases;
	int numGasStations;
	int numEnemyGasStations;
	int numHiddenEnemyGasStations;
	int numFortifications;
	int numEnemyFortifications;
	int numHiddenEnemyFortifications;

	void pickScout();
	void stopScouting();
	void giveScoutOrders();
	int getScoutID();
};

#endif