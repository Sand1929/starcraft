#include <BWAPI.h>
#include <queue>

#ifndef BUILDORDERMANAGER_H
#define BUILDORDERMANAGER_H

class BuildOrderAction
{
public:
	BWAPI::UnitType unit; // the unit type if we're making a unit
	BWAPI::UpgradeType upgrade;
	BWAPI::TechType tech;

	BWAPI::UnitType builder; // unit performing the action
	bool builderOnMinerals; // whether unit is collecting minerals
	bool builderOnGas;

	int mineralCost; // mineral cost of this action
	int gasCost;
	int supplyCost;
	int supplyGain;
	int timeCost;

	std::map<BWAPI::UnitType, int> requiredUnits; // units needed for this action

	BuildOrderAction(BWAPI::UnitType unit = BWAPI::UnitTypes::None, BWAPI::UpgradeType upgrade = BWAPI::UpgradeTypes::None, BWAPI::TechType tech = BWAPI::TechTypes::None);
};

class MacroGameState
{
	std::vector<BuildOrderAction> getTwoGateZealotRushActions();
	std::vector<BuildOrderAction> getDTDropActions();
	std::vector<BuildOrderAction> getThreeGateRoboActions();
	std::vector<BuildOrderAction> getCorsairDarkTemplarActions();
	std::vector<BuildOrderAction> getSpeedZealActions();

public:
	int gameTime;
	float minerals;
	float gas;
	int supplyUsed;
	int supplyTotal;

	int mineralWorkers; // number of workers assigned to minerals
	int gasWorkers;

	std::map<BWAPI::UnitType, int> unitCounts; // how many of each type of unit
	std::vector<std::pair<BWAPI::UnitType, int>> unitsInProgressCounts; // a list of units and their waiting times
	std::map<BWAPI::UpgradeType, int> upgradesInProgressCounts;
	std::map<BWAPI::TechType, int> techsInProgressCounts;
	std::vector<std::pair<BWAPI::UnitType, int>> unitBusyTimes; // a list of units' times until they finish building

	std::map<BWAPI::UnitType, int> goalUnits; // units that are part of our goal
	std::map<BWAPI::UpgradeType, int> goalUpgrades;
	std::set<BWAPI::TechType> goalTechs;

	MacroGameState(std::map<BWAPI::UnitType, int> goalUnits, std::map<BWAPI::UpgradeType, int> goalUpgrades, std::set<BWAPI::TechType> goalTechs);
	MacroGameState(const MacroGameState &currentState, BuildOrderAction action);
	MacroGameState(const MacroGameState &currentState, int delay);

	bool isGoalState();
	std::vector<BuildOrderAction> getFeasibleActions();
	bool hasBuildPotential(BWAPI::UnitType unit);
	bool hasBuildPotential(BWAPI::UpgradeType unit);
	int buildTimeHeuristic();
	int getResourceTime(int totalMineralsRequired, int totalGasRequired);
};

class BuildOrderNode
{
public:
	int timeCost; // current game time in frames
	int heuristic;
	std::deque<BuildOrderAction> actions; // actions we're considering for build order
	MacroGameState state;

	BuildOrderNode(int timeCost, int heuristic, std::deque<BuildOrderAction> actions, const MacroGameState &state) : timeCost(timeCost), heuristic(heuristic), actions(actions), state(state)
	{

	}
};

class NodeComparison
{
public:
	bool operator() (const BuildOrderNode &lhs, const BuildOrderNode &rhs) const
	{
		if (lhs.timeCost + lhs.heuristic != rhs.timeCost + rhs.heuristic) {
			return lhs.timeCost + lhs.heuristic > rhs.timeCost + rhs.heuristic;
		}
		else {
			return lhs.state.minerals + lhs.state.gas < rhs.state.minerals + rhs.state.gas;
		}
	}
};

class BuildOrderManager
{
	BuildOrderManager() {}
	std::deque<BuildOrderAction> buildOrder;
	bool buildUnit(BuildOrderAction action);
	bool buildUpgrade(BuildOrderAction action);

public:
	static BuildOrderManager &getInstance()
	{
		static BuildOrderManager instance;
		return instance;
	}
	BuildOrderManager(const BuildOrderManager &) = delete;
	void operator=(const BuildOrderManager &) = delete;

	void findBuildOrder(std::map<BWAPI::UnitType, int> goalUnits, std::map<BWAPI::UpgradeType, int> goalUpgrades, std::set<BWAPI::TechType> goalTechs);
	void executeBuildOrder();
};

#endif