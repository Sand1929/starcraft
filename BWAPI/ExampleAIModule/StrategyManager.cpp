#include <fstream>
#include "StrategyManager.h"
#include "ResourceManager.h"
#include "ScoutManager.h"
#include "ExpansionManager.h"

using namespace BWAPI;

std::map<UnitType, int> StrategyManager::getTwoGateZealotUnitGoals()
{
	std::map<UnitType, int> result;
	// first, make 6 more probes
	if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Probe) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Probe) < 10)
	{
		result[UnitTypes::Protoss_Probe] = 6;
	}
	// next, make a gateway
	else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Gateway) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Gateway) < 1)
	{
		result[UnitTypes::Protoss_Gateway] = 1;
	}
	// next, two more probes
	else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Probe) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Probe) < 12)
	{
		result[UnitTypes::Protoss_Probe] = 2;
	}
	// next, another gateway
	else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Gateway) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Gateway) < 2)
	{
		result[UnitTypes::Protoss_Gateway] = 1;
	}
	// another probe
	else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Probe) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Probe) < 13)
	{
		result[UnitTypes::Protoss_Probe] = 1;
	}
	// the first zealot
	else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Zealot) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Zealot) < 1) {
		result[UnitTypes::Protoss_Zealot] = 1;
	}
	// another probe
	else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Probe) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Probe) < 14)
	{
		result[UnitTypes::Protoss_Probe] = 1;
	}
	// finally, spam zealots
	else
	{
		result[UnitTypes::Protoss_Zealot] = 2;
		result[UnitTypes::Protoss_Probe] = 1;
		if (Broodwar->self()->minerals() > 100 * std::max(2, Broodwar->self()->allUnitCount(UnitTypes::Protoss_Gateway)))
		{
			result[UnitTypes::Protoss_Gateway] = 1;
		}
	}
	return result;
}

std::map<UnitType, int> StrategyManager::getOneGateCoreUnitGoals()
{
	std::map<UnitType, int> result;
	// first, make 3 more probes
	if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Probe) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Probe) < 7)
	{
		result[UnitTypes::Protoss_Probe] = 3;
	}
	// next, make a gateway, plus more probes
	else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Gateway) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Gateway) < 1)
	{
		result[UnitTypes::Protoss_Gateway] = 1;
		result[UnitTypes::Protoss_Probe] = 3;
	}
	// next, an assimilator
	else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Assimilator) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Assimilator) < 1)
	{
		result[UnitTypes::Protoss_Probe] = 2;
		result[UnitTypes::Protoss_Assimilator] = 1;
	}
	// next, a cybernetics core
	else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Cybernetics_Core) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Cybernetics_Core) < 1)
	{
		result[UnitTypes::Protoss_Probe] = 3;
		result[UnitTypes::Protoss_Cybernetics_Core] = 1;
	}
	// finally, spam dragoons and zealots
	else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Zealot) < Broodwar->self()->allUnitCount(UnitTypes::Protoss_Dragoon))
	{
		result[UnitTypes::Protoss_Zealot] = 1;
		result[UnitTypes::Protoss_Probe] = 2;
	}
	else if (Broodwar->self()->completedUnitCount(UnitTypes::Protoss_Cybernetics_Core) >= 1)
	{
		result[UnitTypes::Protoss_Dragoon] = 1;
	}
	else
	{
		result[UnitTypes::Protoss_Probe] = 1;
	}
	return result;
}

std::map<UnitType, int> StrategyManager::getThreeGateRoboUnitGoals()
{
	std::map<UnitType, int> result;
	if (!getThreeGateRoboUpgradeGoals().empty())
	{
		return result;
	}
	// first, make 6 more probes
	if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Probe) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Probe) < 10)
	{
		result[UnitTypes::Protoss_Probe] = 6;
	}
	// next, make a gateway
	else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Gateway) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Gateway) < 1)
	{
		result[UnitTypes::Protoss_Gateway] = 1;
	}
	// next, an assimilator and two more probes
	else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Assimilator) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Assimilator) < 1)
	{
		result[UnitTypes::Protoss_Probe] = 2;
		result[UnitTypes::Protoss_Assimilator] = 1;
	}
	// next, a cybernetics core
	else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Cybernetics_Core) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Cybernetics_Core) < 1)
	{
		result[UnitTypes::Protoss_Cybernetics_Core] = 1;
	}
	// a zealot and 2 probes
	else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Zealot) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Zealot) < 1)
	{
		Broodwar << "want zealots" << std::endl;
		result[UnitTypes::Protoss_Probe] = 2;
		result[UnitTypes::Protoss_Zealot] = 1;
	}
	// a dragoon and a probe
	else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Dragoon) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Dragoon) < 1)
	{
		result[UnitTypes::Protoss_Probe] = 1;
		result[UnitTypes::Protoss_Dragoon] = 1;
	}
	// then robotics facility
	else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Robotics_Facility) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Robotics_Facility) < 1)
	{
		result[UnitTypes::Protoss_Probe] = 2;
		result[UnitTypes::Protoss_Robotics_Facility] = 1;
	}
	// then two gateways
	else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Gateway) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Gateway) < 3)
	{
		result[UnitTypes::Protoss_Gateway] = 2;
	}
	// a dragoon and a probe
	else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Dragoon) < 2)
	{
		result[UnitTypes::Protoss_Probe] = 1;
		result[UnitTypes::Protoss_Dragoon] = 1;
	}
	// observatory
	else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Observatory) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Observatory) < 1)
	{
		result[UnitTypes::Protoss_Observatory] = 1;
	}
	// finally, spam dragoons and observers
	else
	{
		result[UnitTypes::Protoss_Dragoon] = 3;
		result[UnitTypes::Protoss_Observer] = 1;
	}
	return result;
}

std::map<UpgradeType, int> StrategyManager::getThreeGateRoboUpgradeGoals()
{
	std::map<UpgradeType, int> result;
	if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Dragoon) > 0 && Broodwar->self()->getUpgradeLevel(UpgradeTypes::Singularity_Charge) == 0 && !Broodwar->self()->isUpgrading(UpgradeTypes::Singularity_Charge))
	{
		result[UpgradeTypes::Singularity_Charge] += 1;
	}
	return result;
}

std::map<UnitType, int> StrategyManager::getDTDropUnitGoals()
{
	std::map<UnitType, int> result;
	// first, make 3 more probes
	if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Probe) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Probe) < 10)
	{
		result[UnitTypes::Protoss_Probe] = 6;
	}
	// next, make a gateway
	else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Gateway) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Gateway) < 1)
	{
		result[UnitTypes::Protoss_Gateway] = 1;
		//ScoutManager::getInstance().pickScout();
	}
	// next, an assimilator and two more probes
	else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Assimilator) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Assimilator) < 1)
	{
		result[UnitTypes::Protoss_Assimilator] = 1;
	}
	else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Zealot) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Zealot) < 1)
	{
		result[UnitTypes::Protoss_Probe] = 2;
		result[UnitTypes::Protoss_Zealot] = 1;
	}
	// next, a cybernetics core
	else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Cybernetics_Core) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Cybernetics_Core) < 1)
	{
		result[UnitTypes::Protoss_Cybernetics_Core] = 1;
	}
	else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Zealot) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Zealot) < 2)
	{
		result[UnitTypes::Protoss_Probe] = 2;
		result[UnitTypes::Protoss_Zealot] = 1;
	}
	// next, a citadel
	else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Citadel_of_Adun) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Citadel_of_Adun) < 1)
	{
		result[UnitTypes::Protoss_Citadel_of_Adun] = 1;
	}
	else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Zealot) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Zealot) < 3)
	{
		result[UnitTypes::Protoss_Probe] = 2;
		result[UnitTypes::Protoss_Zealot] = 1;
	}
	/*
	// then robotics facility
	else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Robotics_Facility) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Robotics_Facility) < 1)
	{
	result[UnitTypes::Protoss_Probe] = 2;
	result[UnitTypes::Protoss_Robotics_Facility] = 1;
	}
	*/
	// then templar archives and gateway
	else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Templar_Archives) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Templar_Archives) < 1)
	{
		result[UnitTypes::Protoss_Templar_Archives] = 1;
		result[UnitTypes::Protoss_Gateway] = 1;
	}
	else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Zealot) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Zealot) < 4)
	{
		result[UnitTypes::Protoss_Probe] = 2;
		result[UnitTypes::Protoss_Zealot] = 1;
	}
	/*
	// then shuttle
	else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Shuttle) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Shuttle) < 1)
	{
	result[UnitTypes::Protoss_Probe] = 1;
	result[UnitTypes::Protoss_Shuttle] = 1;
	}
	*/
	// then spam dark templars
	else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Dark_Templar) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Dark_Templar) < 2)
	{
		result[UnitTypes::Protoss_Dark_Templar] = 1;
	}
	// finally, spam zealots
	else if (Broodwar->self()->completedUnitCount(UnitTypes::Protoss_Templar_Archives) >= 1)
	{
		result[UnitTypes::Protoss_Zealot] = 2;
		if (Broodwar->self()->minerals() > 150 * std::max(2, Broodwar->self()->allUnitCount(UnitTypes::Protoss_Gateway)))
		{
			if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Forge) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Forge) < 1)
			{
				result[UnitTypes::Protoss_Forge] = 1;
			}
			else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Photon_Cannon) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Photon_Cannon) < 1)
			{
				result[UnitTypes::Protoss_Photon_Cannon] = 1;
			}
			else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Robotics_Facility) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Robotics_Facility) < 1)
			{
				result[UnitTypes::Protoss_Robotics_Facility] = 1;
			}
			else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Observatory) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Observatory) < 1)
			{
				result[UnitTypes::Protoss_Observatory] = 1;
			}
			if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Observatory) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Observatory) >= 1 && Broodwar->self()->allUnitCount(UnitTypes::Protoss_Gateway) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Gateway) < 4)
			{
				Broodwar << "Need Gateway" << std::endl;
				result[UnitTypes::Protoss_Gateway] = 1;
			}
			else
			{
				Broodwar << "Gateways: " << Broodwar->self()->allUnitCount(UnitTypes::Protoss_Gateway) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Gateway) << std::endl;
			}
		}
		if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Observer) < 1 && Broodwar->self()->allUnitCount(UnitTypes::Protoss_Observatory) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Observatory) >= 1)
		{
			Broodwar << "Need Observer" << std::endl;
			result[UnitTypes::Protoss_Observer] = 1;
		}
	}
	else
	{
		result[UnitTypes::Protoss_Probe] = 1;
	}
	return result;
}

std::map<UpgradeType, int> StrategyManager::getDTDropUpgradeGoals()
{
	std::map<UpgradeType, int> result;
	if (Broodwar->self()->completedUnitCount(UnitTypes::Protoss_Citadel_of_Adun) >= 1 && Broodwar->self()->getUpgradeLevel(UpgradeTypes::Leg_Enhancements) == 0 && !Broodwar->self()->isUpgrading(UpgradeTypes::Leg_Enhancements))
	{
		result[UpgradeTypes::Leg_Enhancements] = 1;
	}
	if (Broodwar->self()->completedUnitCount(UnitTypes::Protoss_Observatory) >= 1 && Broodwar->self()->getUpgradeLevel(UpgradeTypes::Gravitic_Boosters) == 0 && !Broodwar->self()->isUpgrading(UpgradeTypes::Gravitic_Boosters))
	{
		result[UpgradeTypes::Gravitic_Boosters] = 1;
	}
	return result;
}

std::map<UnitType, int> StrategyManager::getCorsairDarkTemplarUnitGoals()
{
	std::map<UnitType, int> result;
	// first, make 3 more probes
	if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Probe) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Probe) < 10)
	{
		result[UnitTypes::Protoss_Probe] = 6;
	}
	// next, make a gateway
	else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Gateway) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Gateway) < 1)
	{
		result[UnitTypes::Protoss_Gateway] = 1;
		ScoutManager::getInstance().pickScout();
	}
	// next, an assimilator and two more probes
	else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Assimilator) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Assimilator) < 1)
	{
		result[UnitTypes::Protoss_Assimilator] = 1;
	}
	else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Zealot) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Zealot) < 1)
	{
		result[UnitTypes::Protoss_Probe] = 2;
		result[UnitTypes::Protoss_Zealot] = 1;
	}
	// next, a cybernetics core
	else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Cybernetics_Core) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Cybernetics_Core) < 1)
	{
		result[UnitTypes::Protoss_Cybernetics_Core] = 1;
	}
	else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Zealot) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Zealot) < 2)
	{
		result[UnitTypes::Protoss_Probe] = 2;
		result[UnitTypes::Protoss_Zealot] = 1;
	}
	// next, a citadel
	else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Citadel_of_Adun) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Citadel_of_Adun) < 1)
	{
		result[UnitTypes::Protoss_Citadel_of_Adun] = 1;
	}
	else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Zealot) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Zealot) < 3)
	{
		result[UnitTypes::Protoss_Probe] = 2;
		result[UnitTypes::Protoss_Zealot] = 1;
	}
	// then templar archives and gateway
	else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Templar_Archives) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Templar_Archives) < 1)
	{
		result[UnitTypes::Protoss_Templar_Archives] = 1;
		result[UnitTypes::Protoss_Gateway] = 1;
	}
	else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Zealot) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Zealot) < 4)
	{
		result[UnitTypes::Protoss_Probe] = 2;
		result[UnitTypes::Protoss_Zealot] = 1;
	}
	// then spam dark templars
	else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Dark_Templar) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Dark_Templar) < 2)
	{
		result[UnitTypes::Protoss_Dark_Templar] = 1;
	}
	// expand
	//else if (ExpansionManager::getInstance().getNumExpansions() == 0)
	//{
	//	Broodwar << "want to expand" << std::endl;
	//	result[UnitTypes::Protoss_Nexus] = 1;
	//}
	// finally, spam zealots
	else if (Broodwar->self()->completedUnitCount(UnitTypes::Protoss_Templar_Archives) >= 1)
	{
		result[UnitTypes::Protoss_Zealot] = 2;
		if (Broodwar->self()->minerals() > 150 * std::max(2, Broodwar->self()->allUnitCount(UnitTypes::Protoss_Gateway)))
		{
			if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Stargate) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Stargate) < 1)
			{
				result[UnitTypes::Protoss_Stargate] = 1;
			}
			else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Robotics_Facility) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Robotics_Facility) < 1)
			{
				result[UnitTypes::Protoss_Robotics_Facility] = 1;
			}
			else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Observatory) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Observatory) < 1)
			{
				result[UnitTypes::Protoss_Observatory] = 1;
			}
			else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Observer) < 1)
			{
				Broodwar << "Need Observer" << std::endl;
				result[UnitTypes::Protoss_Observer] = 1;
			}
			if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Observatory) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Observatory) >= 1 && Broodwar->self()->allUnitCount(UnitTypes::Protoss_Gateway) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Gateway) < 4)
			{
				Broodwar << "Need Gateway" << std::endl;
				result[UnitTypes::Protoss_Gateway] = 1;
			}
			else
			{
				Broodwar << "Gateways: " << Broodwar->self()->allUnitCount(UnitTypes::Protoss_Gateway) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Gateway) << std::endl;
			}
		}
		if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Observer) < 1 && Broodwar->self()->allUnitCount(UnitTypes::Protoss_Observatory) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Observatory) >= 1)
		{
			Broodwar << "Need Observer" << std::endl;
			result[UnitTypes::Protoss_Observer] = 1;
		}
		if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Corsair) < 1 && Broodwar->self()->allUnitCount(UnitTypes::Protoss_Stargate) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Stargate) >= 1)
		{
			result[UnitTypes::Protoss_Corsair] = 1;
		}
	}
	else
	{
		result[UnitTypes::Protoss_Probe] = 1;
	}
	return result;
}

std::map<UpgradeType, int> StrategyManager::getCorsairDarkTemplarUpgradeGoals()
{
	std::map<UpgradeType, int> result;
	if (Broodwar->self()->completedUnitCount(UnitTypes::Protoss_Citadel_of_Adun) >= 1 && Broodwar->self()->getUpgradeLevel(UpgradeTypes::Leg_Enhancements) == 0 && !Broodwar->self()->isUpgrading(UpgradeTypes::Leg_Enhancements))
	{
		result[UpgradeTypes::Leg_Enhancements] = 1;
	}
	if (Broodwar->self()->completedUnitCount(UnitTypes::Protoss_Observatory) >= 1 && Broodwar->self()->getUpgradeLevel(UpgradeTypes::Gravitic_Boosters) == 0 && !Broodwar->self()->isUpgrading(UpgradeTypes::Gravitic_Boosters))
	{
		result[UpgradeTypes::Gravitic_Boosters] = 1;
	}
	return result;
}

std::map<UnitType, int> StrategyManager::getSpeedZealUnitGoals()
{
	std::map<UnitType, int> result;
	// first, make 6 more probes
	if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Probe) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Probe) < 10)
	{
		result[UnitTypes::Protoss_Probe] = 6;
	}
	// next, make a gateway
	else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Gateway) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Gateway) < 1)
	{
		result[UnitTypes::Protoss_Gateway] = 1;
	}
	// next, an assimilator and two more probes
	else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Assimilator) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Assimilator) < 1)
	{
		result[UnitTypes::Protoss_Probe] = 2;
		result[UnitTypes::Protoss_Assimilator] = 1;
	}
	// next, a cybernetics core
	else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Cybernetics_Core) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Cybernetics_Core) < 1)
	{
		result[UnitTypes::Protoss_Cybernetics_Core] = 1;
		result[UnitTypes::Protoss_Zealot] = 1;
		result[UnitTypes::Protoss_Probe] = 2;
	}
	// next, a citadel
	else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Citadel_of_Adun) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Citadel_of_Adun) < 1)
	{
		result[UnitTypes::Protoss_Citadel_of_Adun] = 1;
		result[UnitTypes::Protoss_Zealot] = 1;
		result[UnitTypes::Protoss_Probe] = 2;
	}
	// then another gateway
	else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Gateway) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Gateway) < 2)
	{
		result[UnitTypes::Protoss_Zealot] = 1;
		result[UnitTypes::Protoss_Gateway] = 1;
		result[UnitTypes::Protoss_Probe] = 2;
	}
	// finally, spam zealots
	else if (Broodwar->self()->completedUnitCount(UnitTypes::Protoss_Gateway) >= 2)
	{
		result[UnitTypes::Protoss_Zealot] = 2;
		if (Broodwar->self()->minerals() > 150 * std::max(2, Broodwar->self()->allUnitCount(UnitTypes::Protoss_Gateway)))
		{
			/*
			if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Forge) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Forge) < 1)
			{
				result[UnitTypes::Protoss_Forge] = 1;
			}
			else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Photon_Cannon) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Photon_Cannon) < 1)
			{
				result[UnitTypes::Protoss_Photon_Cannon] = 1;
			}
			else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Robotics_Facility) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Robotics_Facility) < 1)
			{
				result[UnitTypes::Protoss_Robotics_Facility] = 1;
			}
			else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Observatory) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Observatory) < 1)
			{
				result[UnitTypes::Protoss_Observatory] = 1;
			}
			else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Observer) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Observer) < 1)
			{
				result[UnitTypes::Protoss_Observer] = 1;
			}
			else if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Gateway) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Gateway) < 4)
			{
				result[UnitTypes::Protoss_Gateway] = 1;
			}
			*/
		}
	}
	else
	{
		result[UnitTypes::Protoss_Probe] = 1;
	}
	return result;
}

std::map<UpgradeType, int> StrategyManager::getSpeedZealUpgradeGoals()
{
	std::map<UpgradeType, int> result;
	if (Broodwar->self()->completedUnitCount(UnitTypes::Protoss_Citadel_of_Adun) >= 1 && Broodwar->self()->getUpgradeLevel(UpgradeTypes::Leg_Enhancements) == 0 && !Broodwar->self()->isUpgrading(UpgradeTypes::Leg_Enhancements))
	{
		result[UpgradeTypes::Leg_Enhancements] = 1;
	}
	return result;
}

std::map<UnitType, int> StrategyManager::getAirForceGoals()
{
	std::map<UnitType, int> result;
	if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Stargate) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Stargate) < 1)
	{
		result[UnitTypes::Protoss_Stargate] = 1;
	}
	if (Broodwar->self()->allUnitCount(UnitTypes::Protoss_Corsair) < 1 && Broodwar->self()->allUnitCount(UnitTypes::Protoss_Stargate) + ResourceManager::getInstance().getUnitsInProgress(UnitTypes::Protoss_Stargate) >= 1)
	{
		result[UnitTypes::Protoss_Corsair] = 1;
	}
	return result;
}

std::map<BWAPI::UnitType, int> StrategyManager::getUnitGoals()
{
	if (strategy == "TwoGateZealotRush")
	{
		return getTwoGateZealotUnitGoals();
	}
	else if (strategy == "OneGateCore")
	{
		return getOneGateCoreUnitGoals();
	}
	else if (strategy == "ThreeGateRobo")
	{
		return getThreeGateRoboUnitGoals();
	}
	else if (strategy == "CorsairDarkTemplar")
	{
		return getCorsairDarkTemplarUnitGoals();
	}
	else if (strategy == "SpeedZeal")
	{
		return getSpeedZealUnitGoals();
	}
	else
	{
		return getDTDropUnitGoals();
	}
}

std::map<BWAPI::UpgradeType, int> StrategyManager::getUpgradeGoals()
{
	if (strategy == "ThreeGateRobo")
	{
		return getThreeGateRoboUpgradeGoals();
	}
	else if (strategy == "DTDrop")
	{
		return getDTDropUpgradeGoals();
	}
	else if (strategy == "CorsairDarkTemplar")
	{
		return getCorsairDarkTemplarUpgradeGoals();
	}
	else if (strategy == "SpeedZeal")
	{
		return getSpeedZealUpgradeGoals();
	}
	else
	{
		std::map<UpgradeType, int> goalUpgrades;
		return goalUpgrades;
	}
}

std::string StrategyManager::getStrategy()
{
	return strategy;
}

void StrategyManager::decideStrategy()
{
	// read in csv containing table of wins and total games
	const int numStrats = 3;
	const int numRaces = 4;
	const int numMapSizes = 4;
	wins.clear();
	totals.clear();
	std::ifstream winsFile("C:\\Users\\suhsa\\Source\\Repos\\starcraft\\BWAPI\\Release\\wins.txt");
	int x;
	while (winsFile >> x)
	{
		wins.push_back(x);
	}
	winsFile.close();

	std::ifstream totalsFile("C:\\Users\\suhsa\\Source\\Repos\\starcraft\\BWAPI\\Release\\totals.txt");
	while (totalsFile >> x)
	{
		totals.push_back(x);
	}
	totalsFile.close();
	
	// get indices corresponding to race and map size
	int race;
	int mapSize;

	if (Broodwar->enemy()->getRace() == Races::Protoss)
	{
		race = 0;
	}
	else if (Broodwar->enemy()->getRace() == Races::Terran)
	{
		race = 1;
	}
	else if (Broodwar->enemy()->getRace() == Races::Zerg)
	{
		race = 2;
	}
	else
	{
		race = 3;
	}

	if (Broodwar->mapHeight() * Broodwar->mapWidth() <= 64 * 64)
	{
		mapSize = 0;
	}
	else if (Broodwar->mapHeight() * Broodwar->mapWidth() <= 96 * 96)
	{
		mapSize = 1;
	}
	else if (Broodwar->mapHeight() * Broodwar->mapWidth() <= 96 * 128)
	{
		mapSize = 2;
	}
	else
	{
		mapSize = 3;
	}

	// pick the strategy with the highest win rate
	int bestStrategy = 0;
	double bestWinRate = -1;
	double bestTotal = 0;
	double winRate;
	int total;
	
	for (int i = 0; i < numStrats; ++i)
	{
		total = std::max(totals[i * numRaces * numMapSizes + race * numMapSizes + mapSize], 1);
		winRate = (float)wins[i * numRaces * numMapSizes + race * numMapSizes + mapSize] / total;
		// prioritize higher win rates, then less-tried strategies
		if (winRate > bestWinRate || (winRate == bestWinRate && total < bestTotal))
		{
			bestStrategy = i;
			bestWinRate = winRate;
			bestTotal = total;
		}
	}

	if (bestStrategy == 0)
	{
		strategy = "TwoGateZealotRush";
	}
	else if (bestStrategy == 1)
	{
		strategy = "DTDrop";
	}
	else if (bestStrategy == 2)
	{
		strategy = "CorsairDarkTemplar";
	}
}

void StrategyManager::updateStrategy(bool gameWon)
{
	const int numStrats = 3;
	const int numRaces = 4;
	const int numMapSizes = 4;
	// get strategy, race, and map size
	int chosenStrategy;
	int race;
	int mapSize;

	if (strategy == "TwoGateZealotRush")
	{
		chosenStrategy = 0;
	}
	else if (strategy == "DTDrop")
	{
		chosenStrategy = 1;
	}
	else
	{
		chosenStrategy = 2;
	}

	if (Broodwar->enemy()->getRace() == Races::Protoss)
	{
		race = 0;
	}
	else if (Broodwar->enemy()->getRace() == Races::Terran)
	{
		race = 1;
	}
	else if (Broodwar->enemy()->getRace() == Races::Zerg)
	{
		race = 2;
	}
	else
	{
		race = 3;
	}

	if (Broodwar->mapHeight() * Broodwar->mapWidth() <= 64 * 64)
	{
		mapSize = 0;
	}
	else if (Broodwar->mapHeight() * Broodwar->mapWidth() <= 96 * 96)
	{
		mapSize = 1;
	}
	else if (Broodwar->mapHeight() * Broodwar->mapWidth() <= 96 * 128)
	{
		mapSize = 2;
	}
	else
	{
		mapSize = 3;
	}

	// update the win rate tables
	if (gameWon)
	{
		wins[chosenStrategy * numRaces * numMapSizes + race * numMapSizes + mapSize] += 1;
	}
	totals[chosenStrategy * numRaces * numMapSizes + race * numMapSizes + mapSize] += 1;
	/*
	// write the tables to their respective files
	std::ofstream winsFile("C:\\Users\\suhsa\\Source\\Repos\\starcraft\\BWAPI\\Release\\wins.txt");
	std::ostream_iterator<int> winsIterator(winsFile, " ");
	std::copy(wins.begin(), wins.end(), winsIterator);

	std::ofstream totalsFile("C:\\Users\\suhsa\\Source\\Repos\\starcraft\\BWAPI\\Release\\totals.txt");
	std::ostream_iterator<int> totalsIterator(totalsFile, " ");
	std::copy(totals.begin(), totals.end(), totalsIterator);*/
}
