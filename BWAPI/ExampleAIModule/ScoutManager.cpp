#include "ScoutManager.h"
#include <stdlib.h>

using namespace BWAPI;

ScoutManager::ScoutManager()
{
	scoutID = -1;
	scoutType = UnitTypes::Protoss_Probe;
}

void ScoutManager::pickScout()
{
	// pick a probe to be the scout
	for (auto &u : Broodwar->self()->getUnits())
	{
		if (u->getType() == scoutType && u->exists() && u->isCompleted())
		{
			scoutID = u->getID();
			break;
		}
	}
}

void ScoutManager::stopScouting()
{
	scoutID = -1;
}

int ScoutManager::getScoutID()
{
	return scoutID;
}

void ScoutManager::giveScoutOrders()
{
	// get the scouting unit; if it doesn't exist, pick a unit to scout
	if (scoutID == -1)
	{
		pickScout();
	}
	Unit scout = Broodwar->getUnit(scoutID);
	if (!scout->exists() || !scout->isCompleted())
	{
		pickScout();
	}
	scout = Broodwar->getUnit(scoutID);

	// check if scout is close to dangerous units
	bool inDanger = false;
	for (auto &enemy : scout->getUnitsInRadius(300, Filter::IsEnemy))
	{
		if (enemy->canAttackUnit(scout) && (!enemy->getType().isWorker() || enemy->isAttacking()))
		{
			inDanger = true;
			break;
		}
	}
	if (inDanger)
	{
		// run toward base
		Broodwar << "DANGER" << std::endl;
		scout->move(Position(Broodwar->self()->getStartLocation()));
	}
	else
	{
		// pick an unexplored region to explore
		Position startLocation = Position(Broodwar->self()->getStartLocation());
		Position enemyLocation;
		// iterate over starting positions
		for (auto &location : Broodwar->getStartLocations())
		{
			// if location is not our start location, it's the enemy's, so go there
			if (location != Broodwar->self()->getStartLocation())
			{
				enemyLocation = Position(location);
			}
		}
		Broodwar->drawBoxMap(Position(startLocation),
			Position(Broodwar->enemy()->getStartLocation() + UnitTypes::Protoss_Nexus.tileSize()),
			Colors::Blue);
		if (abs(startLocation.x - enemyLocation.x) > abs(startLocation.y - enemyLocation.y))
		{
			//Broodwar << "SCOUTING 1: " << startLocation.x << " " << enemyLocation.x << " " << Broodwar->mapWidth() << ", " << Broodwar->mapHeight() << std::endl;
			if (startLocation.x < Broodwar->mapWidth() / 2)
			{
				for (int i = 0; i < Broodwar->mapWidth(); ++i)
				{
					if (scout->getPosition().y > Broodwar->mapHeight() / 2)
					{
						for (int j = Broodwar->mapHeight() - 1; j >= 0; --j)
						{
							Position target = Broodwar->getRegionAt(Position(i * 32, j * 32))->getCenter();
							if (!Broodwar->isExplored(TilePosition(target)) && Broodwar->getRegionAt(target)->isAccessible() && scout->hasPath(target))
							{
								scout->move(target);
								return;
							}
						}
					}
					else
					{
						for (int j = 0; j < Broodwar->mapHeight(); ++j)
						{
							Position target = Broodwar->getRegionAt(Position(i * 32, j * 32))->getCenter();
							if (!Broodwar->isExplored(TilePosition(target)) && Broodwar->getRegionAt(target)->isAccessible() && scout->hasPath(target))
							{
								scout->move(target);
								return;
							}
						}
					}
				}
			}
			else
			{
				for (int i = Broodwar->mapWidth() - 1; i >= 0; --i)
				{
					if (scout->getPosition().y > Broodwar->mapHeight() / 2)
					{
						for (int j = Broodwar->mapHeight() - 1; j >= 0; --j)
						{
							Position target = Broodwar->getRegionAt(Position(i * 32, j * 32))->getCenter();
							if (!Broodwar->isExplored(TilePosition(target)) && Broodwar->getRegionAt(target)->isAccessible() && scout->hasPath(target))
							{
								scout->move(target);
								return;
							}
						}
					}
					else
					{
						for (int j = 0; j < Broodwar->mapHeight(); ++j)
						{
							Position target = Broodwar->getRegionAt(Position(i * 32, j * 32))->getCenter();
							if (!Broodwar->isExplored(TilePosition(target)) && Broodwar->getRegionAt(target)->isAccessible() && scout->hasPath(target))
							{
								scout->move(target);
								return;
							}
						}
					}
				}
			}
		}
		else
		{
			//Broodwar << "SCOUTING 2: " << startLocation.y << " " << enemyLocation.y << " " << Broodwar->mapWidth() << ", " << Broodwar->mapHeight() << std::endl;
			if (startLocation.y < Broodwar->mapHeight() / 2)
			{
				for (int j = 0; j < Broodwar->mapHeight(); ++j)
				{
					if (scout->getPosition().x > Broodwar->mapWidth() / 2)
					{
						for (int i = Broodwar->mapWidth() - 1; i >= 0; --i)
						{
							Position target = Broodwar->getRegionAt(Position(i * 32, j * 32))->getCenter();
							if (!Broodwar->isExplored(TilePosition(target)) && Broodwar->getRegionAt(target)->isAccessible() && scout->hasPath(target))
							{
								scout->move(target);
								return;
							}
						}
					}
					else
					{
						for (int i = 0; i < Broodwar->mapHeight(); ++i)
						{
							Position target = Broodwar->getRegionAt(Position(i * 32, j * 32))->getCenter();
							if (!Broodwar->isExplored(TilePosition(target)) && Broodwar->getRegionAt(target)->isAccessible() && scout->hasPath(target))
							{
								scout->move(target);
								return;
							}
						}
					}
				}
			}
			else
			{
				for (int j = Broodwar->mapWidth() - 1; j >= 0; --j)
				{
					if (scout->getPosition().x > Broodwar->mapWidth() / 2)
					{
						for (int i = Broodwar->mapWidth() - 1; i >= 0; --i)
						{
							Position target = Broodwar->getRegionAt(Position(i * 32, j * 32))->getCenter();
							if (!Broodwar->isExplored(TilePosition(target)) && Broodwar->getRegionAt(target)->isAccessible() && scout->hasPath(target))
							{
								scout->move(target);
								return;
							}
						}
					}
					else
					{
						for (int i = 0; i < Broodwar->mapWidth(); ++i)
						{
							Position target = Broodwar->getRegionAt(Position(i * 32, j * 32))->getCenter();
							if (!Broodwar->isExplored(TilePosition(target)) && Broodwar->getRegionAt(target)->isAccessible() && scout->hasPath(target))
							{
								scout->move(target);
								return;
							}
						}
					}
				}
			}
		}
	}
}