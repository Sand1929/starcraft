#include <BWAPI.h>

#ifndef COMBATMANAGER_H
#define COMBATMANAGER_H

class EnemyPosition
{
public:
	BWAPI::Position position;
	float strength; // currently calculated as product of total hp * total damage output of enemies at this position
	int timeRecorded; // time when position was recorded, in frames

	EnemyPosition(BWAPI::Position position, float strength, int timeRecorded) : position(position), strength(strength), timeRecorded(timeRecorded)
	{

	}
};

class CombatManager
{
	CombatManager();
	bool cleanUpMode;
	bool increaseSquadSize;
	bool shouldRetreat(BWAPI::Unit unit);
	int squadSize;

	void giveWorkerOrders(BWAPI::Unit u, bool collectGas = true);
	void giveStandardAttackOrders(BWAPI::Unit u, bool invisibleUnits = false, int altSquadSize = -1);
	void standardCleanUpModeCheck();
	void giveTwoGateZealotRushOrders();
	void giveSpeedZealOrders();
	void giveOneGateCoreOrders();
	void giveDTDropOrders();
	void giveThreeGateRoboOrders();
	void giveCorsairDarkTemplarOrders();

public:
	static CombatManager &getInstance()
	{
		static CombatManager instance;
		return instance;
	}
	CombatManager(const CombatManager &) = delete;
	void operator=(const CombatManager &) = delete;

	std::unordered_set<BWAPI::Region> regionsChecked;
	std::set<BWAPI::Position> detectors;
	std::map<BWAPI::Position, BWAPI::UnitType> enemyFortifications;
	std::vector<EnemyPosition> enemyPositions;

	void giveOrders();
	void toggleIncreaseSquadSize();
	int getSquadSize();
};

#endif